<?php
ob_start();
require_once '../config/connect.php';
$name = htmlspecialchars($_POST['name']);
$age = htmlspecialchars($_POST['age']);
$contact = htmlspecialchars($_POST['contact']);
$gender = htmlspecialchars($_POST['gender']);
$address = htmlspecialchars($_POST['address']);
$status = htmlspecialchars($_POST['status']);
$occupation = htmlspecialchars($_POST['occupation']);
$date = date("Y-m-d");
$time = date("h:i:sa");

// Query to insert patient profile using prepared statements
$stmt = $con->prepare("INSERT INTO patient_profile 
				(
				patient_name,
				patient_address,
				patient_contact,
				patient_age,
				patient_gender,
				patient_occupation,
				status,
				date,
				time
				) 
				VALUES (?,?,?,?,?,?,?,?,?)");

$stmt->bind_param("sssssssss", $name,$address,$contact,$age,$gender,$occupation,$status,$date,$time);
$stmt->execute();



$stmt->close();

header("Location:../user/admindashboard.php?action=displaypatients");



?>