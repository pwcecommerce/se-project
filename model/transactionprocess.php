<?php
ob_start();
date_default_timezone_set('Asia/Manila');
session_start();
require_once '../config/connect.php';

$patient_id = (int)htmlspecialchars($_POST['patient_id']);
//$procedure_id =(int) htmlspecialchars($_POST['procedure_id']);
$complain = htmlspecialchars($_POST['complain']);
$bill = (int)htmlspecialchars($_POST['bill']);

$encoder_id = (int)htmlspecialchars($_POST['encoder_id']);
$dentist_id = (int)htmlspecialchars($_POST['dentist_id']);
$payment = (int)htmlspecialchars($_POST['payment']);
$receipt_no = htmlspecialchars($_POST['receipt']);
$balance = $bill - $payment;

var_dump($receipt_no);

var_dump($patient_id,$complain,$bill,$encoder_id,$dentist_id);
$time = date("h:i:sa");
$date = date("Y-m-d");
$ddate = date("Y-m-d");

$dates = new DateTime($ddate);
$week = $dates->format("W");


if(!empty($_POST['procedure_id'])){
	foreach ($_POST['procedure_id'] as $procedure_id) {
		if(is_null($procedure_id)){
			$procedure_id = 0;
		}
		$stmt = $con->prepare("INSERT INTO dental_record 
				(
				patient_id,
				procedure_id, 
				dentist_id,
				complain,
				payable,
				date,
				time,
				encoded_by,
				paid,
				balance,
				receipt_no,
				week_no
				) 
				VALUES (?,?,?,?,?,?,?,?,?,?,?,?)");

$stmt->bind_param("iiisissiiiss", $patient_id,$procedure_id,$dentist_id,$complain,$bill,$date,$time,$encoder_id,$payment,$balance,$receipt_no,$week);
$stmt->execute();
	$bill = 0;
	$payment = 0;
	$balance = 0;

		// $multi = $con->prepare("INSERT INTO multi_procedure (procedure_id,receipt_no) 
		// 						VALUES(?,?)");

		// $multi->bind_param("is",$procedure_id,$receipt_no);
		// $multi->execute();
		// $multi->close();
	}
}

else{
	$stmt = $con->prepare("INSERT INTO dental_record 
				(
				patient_id,
				procedure_id, 
				dentist_id,
				complain,
				payable,
				date,
				time,
				encoded_by,
				paid,
				balance,
				receipt_no,
				week_no
				) 
				VALUES (?,?,?,?,?,?,?,?,?,?,?,?)");

$stmt->bind_param("iiisissiiiss", $patient_id,$procedure_id,$dentist_id,$complain,$bill,$date,$time,$encoder_id,$payment,$balance,$receipt_no,$week);
$stmt->execute();
}



echo $week;

$stmt->close();
$url = $_SESSION['current_link'];
header("Location:".$url);


?>