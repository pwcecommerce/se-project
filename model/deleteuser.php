<?php
ob_start();
session_start();
require_once '../config/connect.php';


$id = htmlspecialchars($_GET['id']);
$is_delete = 1;
$stmt = $con->prepare("UPDATE user SET is_delete = ? where id = ?");// flag user 0 if deleted or 1 to show
$stmt->bind_param("ii",$is_delete,$id);
$stmt->execute();
$stmt->close();
$url = $_SESSION['current_link'];
//header("Location:".$url);
