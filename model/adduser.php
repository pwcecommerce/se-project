<?php
ob_start();
require_once '../config/connect.php';
$name = htmlspecialchars($_POST['name']);
$contact = htmlspecialchars($_POST['contact']);

$address = htmlspecialchars($_POST['address']);
$username = htmlspecialchars($_POST['username']);
$password = sha1($_POST['password']);
$role = htmlspecialchars($_POST['role']);
$admin = htmlspecialchars($_POST['admin']);
$is_delete = 0;


$stmt = $con->prepare("INSERT INTO user 
				(
				name,
				contact,
				address,
				username,
				password,
				role,
				is_admin,
				is_delete
				) 
				VALUES (?,?,?,?,?,?,?,?)");

$stmt->bind_param("sssssiii", $name,$contact,$address,$username,$password,$role,$admin,$is_delete);
$stmt->execute();



$stmt->close();

header("Location:../user/settings.php?action=viewuser");



?>