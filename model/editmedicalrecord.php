<?php
ob_start();
session_start();



require_once '../config/connect.php';

$patient_id = (int)htmlspecialchars($_POST['patient_id']);
$record_id = (int)htmlspecialchars($_POST['record_id']);
$procedure_id  =(int)htmlspecialchars( $_POST['procedure_id']);
$dentist_id  =(int) htmlspecialchars($_POST['dentist_id']);
$complain = htmlspecialchars($_POST['complain']);
$bill  = (int)htmlspecialchars($_POST['bill']);
$payment = (int)htmlspecialchars($_POST['payment']);
$receipt = htmlspecialchars($_POST['receipt']);
$balance = $bill - $payment;
// var_dump($patient_id,$record_id,$procedure_id,$dentist_id,$complain,$bill,$payment);
// var_dump($record_id);
// var_dump($receipt);


$stmt = $con -> prepare('UPDATE dental_record SET 
						procedure_id = ?,
						dentist_id = ?,
						complain = ?,
						payable = ?,
						paid = ?,
						balance=?,
						receipt_no=?

						WHERE 
						record_id=?');
	

$stmt -> bind_param('iisiiisi',$procedure_id,
							   $dentist_id,
							   $complain,
							   $bill,
							   $payment,
							   $balance,
							   $receipt,
							   $record_id
							   );

if($stmt -> num_rows === 1){
	$_SESSION['message'] = "Update Success";
}
print_r($stmt);

$stmt -> execute();
$stmt->close();
$url = $_SESSION['current_link'];
header("Location:".$url);
?>