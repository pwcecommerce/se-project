<?php
ob_start();
session_start();



require_once '../config/connect.php';
$id = htmlspecialchars($_POST['id']);
$name = htmlspecialchars($_POST['name']);
$contact = htmlspecialchars($_POST['contact']);
$username = htmlspecialchars($_POST['username']);
$address = htmlspecialchars($_POST['address']);
$role = htmlspecialchars($_POST['role']);
$admin = htmlspecialchars($_POST['admin']);

$stmt = $con -> prepare('UPDATE user SET 
						name = ?,					
						contact=?,
						address=?,
						username=?,
						role=?,
						is_admin=?
						WHERE 
						id=?');
	
//$name = 'Juan Dela';

//var_dump($name,$id,$contact,$occupation);
//$stmt -> bind_param('sssisssi', $name,$address,$contact,$age,$gender,$occupation,$status,$id);
$stmt -> bind_param('ssssiii', $name,$contact,$address,$username,$role,$admin,$id);
print_r($stmt);
if($stmt -> num_rows === 1){
	$_SESSION['message'] = "Update Success";
}

$stmt -> execute();

$url = $_SESSION['current_link'];
header("Location:".$url);
?>