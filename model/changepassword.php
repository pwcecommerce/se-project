<?php
ob_start();
session_start();
require_once '../config/connect.php';

$password = sha1($_POST['password']);
$id = htmlspecialchars($_POST['id']);

//var_dump($id,$password);

$stmt = $con -> prepare('UPDATE user SET 
						
						password=?
						
						WHERE 
						id=?');
$stmt -> bind_param('si', $password,$id);
$stmt -> execute();

$url = $_SESSION['current_link'];
echo "Password Successfully Reset";

header( "refresh:3;".$url);