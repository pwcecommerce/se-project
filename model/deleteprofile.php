<?php
ob_start();
session_start();
require_once '../config/connect.php';


$id = (int)htmlspecialchars($_GET['id']);
var_dump($id);
$stmt = $con->prepare("DELETE from patient_profile where patient_id = ?");
$stmt->bind_param("i",$id);
$stmt->execute();
$stmt->close();

$stmt2 = $con->prepare("DELETE from dental_record where patient_id = ?");
$stmt2->bind_param("i",$id);
$stmt2->execute();
$stmt2->close();

$url = $_SESSION['current_link'];
header("Location:".$url);
