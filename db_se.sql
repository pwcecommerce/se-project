-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 18, 2019 at 08:24 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_se`
--

-- --------------------------------------------------------

--
-- Table structure for table `dental_procedure`
--

CREATE TABLE `dental_procedure` (
  `procedure_id` int(11) NOT NULL,
  `procedure_name` varchar(191) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dental_procedure`
--

INSERT INTO `dental_procedure` (`procedure_id`, `procedure_name`) VALUES
(1, 'Oral examination and Consultation'),
(2, 'Oral Prophylaxis'),
(3, 'Airmax Polishing'),
(4, 'Light cured filling'),
(5, 'Root Canal Treatment'),
(6, 'Crowns'),
(7, 'Dentures'),
(8, 'Surgery');

-- --------------------------------------------------------

--
-- Table structure for table `dental_record`
--

CREATE TABLE `dental_record` (
  `record_id` int(20) NOT NULL,
  `patient_id` int(20) DEFAULT NULL,
  `procedure_id` int(20) DEFAULT NULL,
  `dentist_id` int(20) DEFAULT NULL,
  `complain` varchar(191) DEFAULT NULL,
  `payable` varchar(100) DEFAULT NULL,
  `date` varchar(20) DEFAULT NULL,
  `time` varchar(20) DEFAULT NULL,
  `encoded_by` int(20) DEFAULT NULL,
  `paid` varchar(10) DEFAULT NULL,
  `balance` varchar(20) DEFAULT NULL,
  `receipt_no` varchar(50) DEFAULT NULL,
  `week_no` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dental_record`
--

INSERT INTO `dental_record` (`record_id`, `patient_id`, `procedure_id`, `dentist_id`, `complain`, `payable`, `date`, `time`, `encoded_by`, `paid`, `balance`, `receipt_no`, `week_no`) VALUES
(26, 2, 1, 1, 'Toothache', '1400', '2019-09-15', '11:27:46am', 1, '1000', '400', '09898898', '37'),
(27, 2, 6, 1, 'Toothache', '1400', '2019-09-15', '11:28:29am', 1, '1000', '400', '', '37'),
(28, 2, 1, 1, 'Toothache', '1400', '2019-09-23', '11:28:39am', 1, '1000', '400', NULL, '38'),
(29, 2, 1, 1, 'Toothache', '1400', '2019-09-24', '11:28:45am', 1, '1000', '400', NULL, '38'),
(30, 1, 1, 1, 'Toothache', '1300', '2019-07-15', '11:32:53am', 1, '1000', '300', NULL, '28'),
(31, 8, 1, 1, 'asd', '123', '2019-09-15', '01:30:14pm', 1, '123', '0', NULL, '37'),
(32, 1, 1, 1, 'Toothache', '1500', '2019-09-15', '02:37:08pm', 1, '1000', '500', '878879', '37'),
(33, 1, 1, 1, '', '0', '2019-09-15', '02:37:25pm', 1, '0', '0', 'asdfsd', '37'),
(34, 1, 1, 1, 'Toothache', '1000', '2019-09-16', '08:23:44am', 1, '500', '500', '', '38');

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE `invoice` (
  `invoice_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `total_amount` double NOT NULL,
  `date` varchar(50) NOT NULL,
  `time` varchar(20) NOT NULL,
  `receipt_number` varchar(191) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `patient_profile`
--

CREATE TABLE `patient_profile` (
  `patient_id` int(11) NOT NULL,
  `patient_name` varchar(191) NOT NULL,
  `patient_address` varchar(191) NOT NULL,
  `patient_contact` varchar(20) NOT NULL,
  `patient_age` varchar(10) NOT NULL,
  `patient_gender` varchar(20) NOT NULL,
  `patient_occupation` varchar(191) NOT NULL,
  `status` varchar(20) NOT NULL,
  `date` varchar(50) NOT NULL,
  `time` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `patient_profile`
--

INSERT INTO `patient_profile` (`patient_id`, `patient_name`, `patient_address`, `patient_contact`, `patient_age`, `patient_gender`, `patient_occupation`, `status`, `date`, `time`) VALUES
(1, 'Juan Cruz', 'Davao', '09090909', '23', 'Male', 'Business Owner', 'Married', '9/11/2019', '7:50 AM'),
(2, 'Juan Cena', 'toril', '1234', '39', 'M', 'Engineer', 'Married', '9/11/2019', '9:05'),
(3, 'Diego Dora', '26', '090909', 'M', 'Digos', 'Single', 'Student', '09-14-2019', '04:09:58pm'),
(4, 'Kael Bata', '4', '0909090909', 'M', 'Davao', 'Single', 'Student', '09-15-2019', '03:50:03am'),
(8, 'Test', '89', '9898', 'M', 'asdj', 'afasasd', 'asdlk', '2019-09-15', '07:22:54am'),
(9, 'Test 2', '43', '234', 'M', 'asdj', 'werwer', '324234', '2019-09-15', '07:26:54am'),
(10, '', '', '', '', '', '', '', '2019-09-15', '07:27:32am');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(191) NOT NULL,
  `address` varchar(250) NOT NULL,
  `contact` varchar(20) NOT NULL,
  `username` varchar(191) NOT NULL,
  `role` int(11) NOT NULL,
  `is_admin` int(11) NOT NULL,
  `password` varchar(191) NOT NULL,
  `is_delete` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `address`, `contact`, `username`, `role`, `is_admin`, `password`, `is_delete`) VALUES
(1, 'Ong', 'Davao', '09990999090', 'admin', 1, 1, 'd033e22ae348aeb5660fc2140aec35850c4da997', 1),
(2, 'Staff', 'Davao', '', 'staff', 0, 1, 'd033e22ae348aeb5660fc2140aec35850c4da997', 0),
(3, 'Wu', 'Davao', '909090', 'dentist', 1, 0, 'd033e22ae348aeb5660fc2140aec35850c4da997', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dental_procedure`
--
ALTER TABLE `dental_procedure`
  ADD PRIMARY KEY (`procedure_id`);

--
-- Indexes for table `dental_record`
--
ALTER TABLE `dental_record`
  ADD PRIMARY KEY (`record_id`);

--
-- Indexes for table `patient_profile`
--
ALTER TABLE `patient_profile`
  ADD PRIMARY KEY (`patient_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dental_procedure`
--
ALTER TABLE `dental_procedure`
  MODIFY `procedure_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `dental_record`
--
ALTER TABLE `dental_record`
  MODIFY `record_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `patient_profile`
--
ALTER TABLE `patient_profile`
  MODIFY `patient_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
