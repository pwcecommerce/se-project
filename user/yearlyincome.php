<?php
require_once '../config/connect.php';

?>

<div class="form-group custom-input-space has-feedback">
				<div class="page-heading">
					<h3 class="post-title"></h3>
				</div>
				<div class="page-body clearfix">
					<div class="row">
						<div class="col-md-offset-0 col-md-12">
							<div class="panel panel-default">
								<div class="panel-heading " style="background-color: #5cb85c !important" >
										<center>
										<div class="btn btn-primary" style="background-color: #5cb85c !important;border-color: #5cb85c !important">YEARLY ACCOUNTING REPORT </div>
										</center>

								 	</div>
								
								
								<div class="panel-body">
									<table class="table table-hover table-responsive table-editable" id="dashy">
					    	
					    	<div class="btn-group dropright">
						  <button type="button" class="btn btn-success dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    SELECT YEAR
						  </button>
						  <div class="dropdown-menu ">
						  	<?php
						  	$stmt = $con->prepare("SELECT extract(year FROM date) from dental_record group by extract(year FROM date)");
						  	$stmt->execute();
							$stmt->store_result();
							$stmt->bind_result($dat);
							while($stmt->fetch()) {
						  	echo'

						    <a class="dropdown-item" href="admindashboard.php?action=yearly&year='.$dat.'">'.$dat.'</a>';

						    }
						    $stmt->close();
						    ?>
						   </div>
						   </div> 
					    	<thead>
					    		<tr>
					    		 <th scope="col">YEAR</th>
					    		 <th scope="col">MONTH</th>
					    		 <th scope="col">TOTAL BILL</th>
					    		 <th scope="col">CASH ON HAND</th>
					    		 <th scope="col">ACCOUNTS RECIEVABLE</th>
					    		 
							      
							      
					    		</tr>

					    	</thead>
					    	<tbody>
					    		
					<?php   
					if(isset($_GET['year'])){
						$year = $_GET['year'];
					}
					else{
						$year = date('Y');
					}

	$stmt = $con->prepare("SELECT sum(payable), sum(paid),sum(balance), extract(year FROM date),MONTHNAME(date) from dental_record where extract(year FROM date) = ? group by MONTHNAME(date)");
					$stmt->bind_param("i", $year);
					$stmt->execute();
					$stmt->store_result();
					if($stmt->num_rows === 0) {
						echo "No Data Found";
						
						//header("Location:../user/admindashboard.php");
					}

					$stmt->bind_result(
							$bill,$paid,$balance,$year,$month
						); 
					
					$total_bill = 0;
					$total_balance = 0;
					$total_paid = 0;
				$i = 0;	
				$years = 0;	


while($stmt->fetch()) {
	$total_bill = $total_bill + $bill;
	$total_paid = $total_paid + $paid;
	$total_balance = $total_balance + $balance;
					
$i++;

	
	if($year>$years){
			 $i = 0;
		}
		$years = $year;
					echo"<tr>";
						    	
						    	if($i==0){
						    	echo"<td>$year</td>";
						    	}
						    	else{
						    		echo"<td>-</td>";
						    	}
						    	echo"<td>$month</td>
						    	<td>$bill</td>
						    	<td>$paid</td>
						    	<td>$balance</td>
						    	
					    </tr>
					    			"

					    			;

					}

						echo"<tr>
						    	<th>TOTAL</th>
						    	
						    	<th></th>
						    	<th>".$total_bill."</th>
						    	<th>".$total_paid."</th>
						    	<th>".$total_balance."</th>
						    	
					    </tr>

						";


					$stmt->close();


					



					?>	

								
					    			
					    		
					    	</tbody>
					    
					  </table>



								</div>
							</div>
						</div> 
					</div>
				</div>
			</div>
