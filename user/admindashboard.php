<?php
require_once("../layouts/header.php");



if(isset($_SESSION['is_admin'])){
		$is_admin = $_SESSION['is_admin'];
		
		
	}
	else{
		header("Location:../index.php");
	}

//echo 	date("Y-m-d");

?>


	<?php require_once("../layouts/navbar.php"); 
	      		 
	?>


	<section class="container">
			<div class="form-group custom-input-space has-feedback">
				<div class="page-heading">
					<h3 class="post-title"></h3>
				</div>
				<div class="page-body clearfix">
					<div class="row">
						<div class="col-md-offset-0 col-md-12">
							<div class="panel panel-default">
								<div class="panel-heading " style="background-color: #5cb85c !important" >
										<center>
										<div class="btn btn-success" style="background-color: #5cb85c !important;">Admin Dashboard </div>
										</center>

								 	</div>
								
								
								<div class="panel-body">
									<div class="col-sm-3 pt-4">

					 
					    		<div class="col-md-offset-0 col-md-12 pt-3">
<!-- Side Bar Options -->
					   <div class="active-cyan-4 mb-3 ">
					    <form class="form-inline" method="post" action="../model/getuser.php">	
 						<input class="form-control col-sm-8 mb-3" type="text" placeholder="Search Patient"  id="q" name="q" aria-label="Search" >
 						 <button type="submit" class="btn btn-success col-sm-4 col-sm-offset-0 mb-3">Search</button>
 						</form>

 	  <a href="admindashboard.php?action=addpatient" class="btn btn-success col-md-offset-0  col-md-12 mb-3"><label>Add Patient</label></a><br/>
      <a href="admindashboard.php?action=displaypatients" class="btn btn-success col-md-offset-0  col-md-12 mb-3"><label>Display Patients</label></a><br/>
      

      
      	
      <a id="dropdownmenu1" class="btn btn-success col-md-offset-0  col-md-12 mb-3 btn-group  dropdown-toggle dropdown dropright" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><label style="color:white">Reports</label></a>
      	<ul aria-labelledby="dropdownMenu1" class="dropdown-menu border-0 shadow">
      		

<!-- ********************Patients ************************************************** -->
	<li class="dropdown-submenu dropright">
            	<a class="dropdown-item" id="dropdownmenu3" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-item dropdown-toggle">Patients</a>
            		<ul aria-labelledby="dropdownMenu3" class="dropdown-menu border-0 shadow">
                <li>
                  <a tabindex="-1" href="admindashboard.php?action=dailypatients" class="dropdown-item">Daily</a>
                </li>
                <li>
                  <a tabindex="-1" href="admindashboard.php?action=weeklypatients" class="dropdown-item">Weekly</a>
                </li>
                <li>
                  <a tabindex="-1" href="admindashboard.php?action=monthlypatients" class="dropdown-item">Monthly</a>
                </li><li>
                  <a tabindex="-1" href="admindashboard.php?action=yearlypatients" class="dropdown-item">Yearly</a>
                </li>
                	</ul>	
            </li>





<!-- ********************8 Accounting ************************************************** -->

            <li class="dropdown-submenu dropright">
            	<a class="dropdown-item" id="dropdownmenu2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-item dropdown-toggle">Accounting</a>
            		<ul aria-labelledby="dropdownMenu2" class="dropdown-menu border-0 shadow">
                <li>
                  <a tabindex="-1" href="admindashboard.php?action=daily" class="dropdown-item">Daily</a>
                </li>
                <li>
                  <a tabindex="-1" href="admindashboard.php?action=weekly" class="dropdown-item">Weekly</a>
                </li>
                <li>
                  <a tabindex="-1" href="admindashboard.php?action=monthly" class="dropdown-item">Monthly</a>
                </li><li>
                  <a tabindex="-1" href="admindashboard.php?action=yearly" class="dropdown-item">Yearly</a>
                </li>
                	</ul>	
            </li>





           <li class="dropdown-submenu dropright">
            	<a class="dropdown-item" id="dropdownmenu4" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-item dropdown-toggle">Procedure</a>
            		<ul aria-labelledby="dropdownMenu4" class="dropdown-menu border-0 shadow">
                <li>
                  <a tabindex="-1" href="admindashboard.php?action=dailyprocedure" class="dropdown-item">Daily</a>
                </li>
                <li>
                  <a tabindex="-1" href="admindashboard.php?action=weeklyprocedure" class="dropdown-item">Weekly</a>
                </li>
                <li>
                  <a tabindex="-1" href="admindashboard.php?action=monthlyprocedures" class="dropdown-item">Monthly</a>
                </li><li>
                  <a tabindex="-1" href="admindashboard.php?action=yearlyprocedures" class="dropdown-item">Yearly</a>
                </li>
                	</ul>	
            </li>

      	</ul>




<a href="admindashboard.php?action=overview" class="btn btn-success col-md-offset-0  col-md-12 mb-3"><label>Total Overview</label></a><br/>



      				<a href="admindashboard.php?action=dentalprocedures" class="btn btn-success col-md-offset-0  col-md-12 mb-3"><label>Add Dental Procedure</label></a><br/>
      	
										</div>
					    				 
					    				 
     			
      
      
    
										 
					    		</div>

					    		
					    	
					    	
					    
					</div>
<!-- End of Side Bar Options -->

<!-- Display Action according to choice -->
					<div class="col-sm-9">
						<div class="col-md-offset-0 col-md-12" >
					    			

										<?php  //foreach( $codes as $index => $code )
											if(isset($_GET['action'])){
												if($_GET['action'] ==='displaypatients')
												{
													require_once('displaypatients.php');
												}
												if($_GET['action'] ==='addpatient')
												{
													
													require_once('addpatient.php');
												}
												if($_GET['action'] ==='daily')
												{
													require_once('dailyincome.php');
												}
												if($_GET['action'] ==='weekly')
												{
													require_once('weeklyincome.php');
												}
												if($_GET['action'] ==='monthly')
												{
													require_once('monthlyincome.php');
												}
												if($_GET['action'] ==='dentalprocedures')
												{
													require_once('dentalprocedures.php');
												}
												if($_GET['action'] ==='yearly')
												{
													require_once('yearlyincome.php');
												}

												if($_GET['action'] ==='dailyprocedure')
												{
													require_once('dailyprocedures.php');
												}

												if($_GET['action'] ==='overview')
												{
													require_once('overview.php');
												}
												if($_GET['action'] ==='weeklyprocedure')
												{
													require_once('weeklyprocedures.php');
												}
												if($_GET['action'] ==='monthlyprocedures')
												{
													require_once('monthlyprocedures.php');
												}
												if($_GET['action'] ==='yearlyprocedures')
												{
													require_once('yearlyprocedure.php');
												}

												if($_GET['action'] ==='dailypatients')
												{
													require_once('dailypatients.php');
												}
												if($_GET['action'] ==='weeklypatients')
												{
													require_once('weeklypatients.php');
												}
												if($_GET['action'] ==='monthlypatients')
												{
													require_once('monthlypatients.php');
												}
												if($_GET['action'] ==='yearlypatients')
												{
													require_once('yearlypatients.php');
												}
												if($_GET['action'] ==='fetch-name')
												{
													require_once('fetchnames.php');
												}

												if($_GET['action'] ==='search' && isset($_SESSION['q']))
												{
													require_once('searchresults.php');
												}

											}
											else{
												require_once('displaypatients.php');
												
											}
											
												

										 ?>
					    		</div>
					</div>
										
				</div>			
								

<!-- End of Displaying Action according to choice -->

						
					</div>


				</div>
				</div>
			</div>
		</section>


		<script type="text/javascript">
			$(function() {
  // ------------------------------------------------------- //
  // Multi Level dropdowns
  // ------------------------------------------------------ //
				  $("ul.dropdown-menu [data-toggle='dropdown']").on("click", function(event) {
				    event.preventDefault();
				    event.stopPropagation();

				    $(this).siblings().toggleClass("show");


				    if (!$(this).next().hasClass('show')) {
				      $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
				    }
				    $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
				      $('.dropdown-submenu .show').removeClass("show");
				    });

				  });
				});
		</script>
</body>



