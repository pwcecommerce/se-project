<?php
require_once '../config/connect.php';

?>
<div class="form-group custom-input-space has-feedback">
				<div class="page-heading">
					<h3 class="post-title"></h3>
				</div>
				<div class="page-body clearfix">
					<div class="row">
						<div class="col-md-offset-0 col-md-12">
							<div class="panel panel-default">
								<div class="panel-heading " style="background-color: #5cb85c !important" >
										<center>
										<div class="btn btn-primary" style="background-color: #5cb85c !important;border-color: #5cb85c !important">Add Transaction </div>
										</center>

								 	</div>
								
								
<div class="panel-body">
	<form class="form-horizontal pb-2" method="post" action="../model/transactionprocess.php">
  		<div class="form-group">
    <?php

    					
    echo'
    <div class="col-sm-12 pb-2">
      <input type="hidden" class="form-control" id="patient_id" name="patient_id" placeholder="Patient ID" value="'.$id.'">
    </div>
    <div class="col-sm-12 pb-2">';
      
     
    echo'</div>

    <div class="col-sm-12 pb-2">
      
      <select class="form-control" id="dentist_id" name="dentist_id">';

      		$role = 1;
      		$stmt = $con->prepare("SELECT id,name FROM user WHERE role = ?");		
			$stmt->bind_param("i", $role);		
			$stmt->execute();
					$stmt->store_result();
					if($stmt->num_rows === 0) {
							echo "No Data";
							//header("Location:../user/admindashboard.php");
						}


						$stmt->bind_result($dentist_id,$dentist_name);
						while($stmt->fetch()) {
									  		
							echo"<option value='".$dentist_id."'  name='".$dentist_id."'>Dr. ".$dentist_name."</option>
        						";	

        						
											}
					$stmt->close();




      
//Query Procedure here

					$stmt = $con->prepare("SELECT * FROM dental_procedure");
					//$stmt->bind_param("i", $id);
					$stmt->execute();
					$stmt->store_result();
					if($stmt->num_rows === 0) {
							echo "No Data";
							//header("Location:../user/admindashboard.php");
						}
						$stmt->bind_result($procedure_id,$procedure_name);
						
						
						while($stmt->fetch()) {
								  		
							echo"
							
							<input type='checkbox' name='procedure_id[]' value='".$procedure_id."'>".$procedure_name."<br/>
							
        						";	


											}
											
					$stmt->close();

        

//End here        
      

     

    echo' 



    </div>



    <div class="col-sm-12 pb-2">
      <input type="text" class="form-control" id="complain" name="complain" placeholder="Enter Complain" value="">
    </div>
    <div class="col-sm-12 pb-2">
      <input type="text" class="form-control" id="bill"  name="bill"  onkeyup="validateNum(this.value)"placeholder="Enter Bill"value="">
    </div>
    <div class="col-sm-12 pb-2">
      <input type="text" class="form-control" id="payment" name="payment" onkeyup="validateNum(this.value)" placeholder="Enter Payment" value="">
    </div>
    <div class="col-sm-12 pb-2">
      <input type="text" class="form-control" id="receipt" name="receipt" placeholder="Enter Receipt" value="">
    </div>
    <div class="col-sm-12 pb-2">
      <input type="hidden" class="form-control" id="encoder_id" name="encoder_id" placeholder="Occupation"
      value="'.$_SESSION['id'].'">
    </div>'
    ;
    ?>
  </div>
  
  
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-success" id="submit" name="submit">Add Transaction</button>
    </div>
  </div>
</form>
								</div>
							</div>
						</div> 
					</div>
				</div>
			</div>

<script type="text/javascript">
					function validateNum(num){
						submit = document.getElementById("submit");
						if(isNaN(num)){
							document.getElementById("submit").disabled = true;
							document.getElementById("submit").innerText= "Invalid Input";


						}
						else{document.getElementById("submit").disabled = false;
								document.getElementById("submit").innerText= "Add Transaction";
							}
					}

			</script>