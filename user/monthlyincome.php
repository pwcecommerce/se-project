<?php
require_once '../config/connect.php';

?>

<div class="form-group custom-input-space has-feedback">
				<div class="page-heading">
					<h3 class="post-title"></h3>
				</div>
				<div class="page-body clearfix">
					<div class="row">
						<div class="col-md-offset-0 col-md-12">
							<div class="panel panel-default">
								<div class="panel-heading " style="background-color: #5cb85c !important" >
										<center>
										<div class="btn btn-primary" style="background-color: #5cb85c !important;border-color: #5cb85c !important">MONTHLY ACCOUNTING REPORT </div>
										</center>

								 	</div>
								
								
								<div class="panel-body">
									<table class="table table-hover table-responsive table-editable" id="dashy">
					    	<div class="btn-group dropright">
						  <button type="button" class="btn btn-success dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    SELECT MONTH
						  </button>
						  <div class="dropdown-menu ">
						  	<?php
						  	$stmt = $con->prepare("SELECT EXTRACT(MONTH FROM date),MONTHNAME(date) from dental_record group by MONTHNAME(date) desc");
						  	$stmt->execute();
							$stmt->store_result();
							$stmt->bind_result($dat,$dat2);
							while($stmt->fetch()) {
						  	echo'

						    <a class="dropdown-item" href="admindashboard.php?action=monthly&month='.$dat.'">'.$dat2.'</a>';

						    }
						    $stmt->close();
						    ?>
						</div>
					</div>
						  
						</v-container>
					    	<thead>
					    		<tr>
					    		 <th scope="col">MONTH</th>
					    		 <th scope="col">WEEK</th>
					    		 <th scope="col">TOTAL BILL</th>
					    		 <th scope="col">RECIEVED PAYMENTS</th>
					    		 <th scope="col">ACCOUNTS RECIEVABLE</th>
							      
							      
					    		</tr>

					    	</thead>
					    	<tbody>
					    		
					<?php
					$total_bill = 0;
					$total_balance = 0;
					$total_paid = 0;
					$month = date('m');
					if(isset($_GET['month'])){
						$month = $_GET['month'];
					}
					else{
						$month = date('m');
					}
										$stmt = $con->prepare("SELECT MONTHNAME(date),week(date),sum(payable),sum(balance),sum(paid) FROM dental_record where EXTRACT(MONTH FROM date) = ? group by week(date)");



					$stmt->bind_param("i", $month);
					$stmt->execute();
					$stmt->store_result();
					if($stmt->num_rows === 0) {
						echo "No Data Found";
						
						//header("Location:../user/admindashboard.php");
					}

					$stmt->bind_result(
						$month,$week,
						$monthly_bill,
						$monthly_balance,
						$monthly_paid
						
						
						
						); 
					$i = 0;	
				
				$months = date_parse('January');
while($stmt->fetch()) {
	$total_bill = $total_bill + $monthly_bill;
	$total_paid = $total_paid + $monthly_paid;
	$total_balance = $total_balance + $monthly_balance;

$i++;
	$monthss = date_parse($month);
	if($monthss['month']>$months['month']){
			 $i = 0;
		}
		$months['month'] = $monthss['month'];//
					echo"<tr>";
						    	
						    	if($i==0){
						    	echo"<td>$month</td>";
						    }
						    	else{
						    		echo"<td>-</td>";
						    	}
						    	
						    	echo"<td>$week</td>
						    	<td>$monthly_bill</td>
						    	<td>$monthly_paid</td>
						    	<td>$monthly_paid</td>
					    </tr>
					    			"

					    			;

					}




					$stmt->close();


					
					echo"
								<tr>
					    			<th>Total</th>
					    			<th>".$total_bill."</th>
					    			<th>".$total_paid."</th>
					    			<th>".$total_balance."</th>
					    		</tr>
					";


					?>	

								
					    			

					    	</tbody>
					    
					  </table>



								</div>
							</div>
						</div> 
					</div>
				</div>
			</div>
