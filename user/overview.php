<?php
require_once '../config/connect.php';

?>
<div class="form-group custom-input-space has-feedback">
				<div class="page-heading">
					<h3 class="post-title"></h3>
				</div>
				<div class="page-body clearfix">
					<div class="row">
						<div class="col-md-offset-0 col-md-12">
							<div class="panel panel-default">
								<div class="panel-heading " style="background-color: #5cb85c !important" >
										<center>
										<div class="btn btn-primary" style="background-color: #5cb85c !important;border-color: #5cb85c !important">


										<b>TOTAL OVERVIEW</b> </div>
										</center>

								 	</div>
								
								
<div class="panel-body">
	
  		<div class="col-sm-3 pt-4">
  			<b>PATIENTS</b>
  			
  				<?php
  				$genM = "Male";
  				$male = 0;
  				$female = 0;
  					$stmt = $con->prepare("SELECT patient_gender
										FROM patient_profile ");
  							//$stmt->bind_param("s",$genM);
  							$stmt->execute();
							$stmt->store_result();
							$stmt->bind_result($patient_gender);
							while($stmt->fetch()){
								
								if($patient_gender === "Male"){
									$male++;
								}
							
							else{
								$female++;
							}

							}
							$stmt->close();

							$total = $male + $female;
							$rateM = ($male/$total)*100;
							$rateF = ($female/$total)*100;
							echo"Male:".number_format($rateM,2,'.','') ."%<br/>";
							echo"Female:".number_format($rateF,2,'.','') ."%<br/>";

  				?>
  			
  		</div>
  		<div class="col-sm-4 pt-4">
  			<b>TOTAL PROCEDURE</b>
  			<?php 

  			$stmt1 = $con->prepare("SELECT count(patient_id) from dental_record ");
						//$stmt1->bind_param("s", $today);
						$stmt1->execute();
					$stmt1->store_result();
					if($stmt1->num_rows === 0) {
						echo "No Data Found";	
					}
					$stmt1->bind_result($total);
					$stmt1->fetch();
					
					$stmt1->close();

			$stmt = $con->prepare("SELECT dental_record.date,COUNT(dental_record.patient_id),dental_record.procedure_id, dental_procedure.procedure_name from dental_record,dental_procedure where dental_record.procedure_id = dental_procedure.procedure_id group by dental_procedure.procedure_name,dental_record.procedure_id");
					//$stmt->bind_param("s", $today);
					$stmt->execute();
					$stmt->store_result();
					if($stmt->num_rows === 0) {
						echo "No Data Found For Today's Date";
						
						//header("Location:../user/admindashboard.php");
					}

					$stmt->bind_result(
						$daily_date,
						$daily_patient_count,
						$procedure_id,
						$daily_procedure
						); 
					
					$total_count = 0;
					$rate = 0;
					$i = 0;
					$d = mktime(8, 12, 2014);
					$daily_dates = "2000-1-12";
					echo"<table class='table table-hover table-responsive '>";
while($stmt->fetch()) {
$i++;
if($daily_date > $daily_dates){
	
	$i = 0;
}

$daily_dates = $daily_date;

	$total_count = $total_count + $daily_patient_count;
	$rate = ($daily_patient_count/$total)*100;
					
						    	
						    	
						    	echo"
						    	<tr><td>
						    	$daily_procedure: </td><td>
						    	
						    	".number_format($rate,2,'.','')."%</td>
						    	
						    	
						    	</tr>
				
					    			"

					    			;

					}

						echo"<tr>
						    	<th>TOTAL</th>
						    	";
						    	if($rate === 0){echo"<th>0%</th>";}
						    		else{echo"<th>100%</th>";}
						    	
						    	
						    	echo"<th></th>
					    </tr>
					    </table>
						";


					$stmt->close();

							?>

  		</div>
  		<div class="col-sm-5 pt-4">
  			<b>TOTAL ACCOUNTING (Pesos)</b>
  			<?php

  					$stmt = $con->prepare("SELECT dental_record.patient_id,dental_record.date,
							  dental_record.time,
			                  dental_record.receipt_no,
							  SUM(dental_record.payable),
							  SUM(dental_record.paid),
							  SUM(dental_record.balance),
						  	  patient_profile.patient_name 
						 from patient_profile,dental_record where patient_profile.patient_id = dental_record.patient_id ");
					//$stmt->bind_param("s", $today);
					$stmt->execute();
					$stmt->store_result();
					if($stmt->num_rows === 0) {
						echo "No Data Found For Current Date";
						
						//header("Location:../user/admindashboard.php");
					}

					$stmt->bind_result(
						$id,
						$date,$time,$receipt_no,
						$payable,
						$paid,
						$balance,
						$patient_name
						); 
					
					$total_bill = 0;
					$total_balance = 0;
					$total_paid = 0;	

					$i = 0;
					$d = mktime(8, 12, 2014);
					$daily_dates = "2000-1-12";
					echo"<table class='table table-hover table-responsive '>
				    ";
while($stmt->fetch()) {
	$total_bill = $total_bill + $payable;
	$total_paid = $total_paid + $paid;
	$total_balance = $total_balance + $balance;

	$i++;
if($date > $daily_dates){
	
	$i = 0;
}
$daily_dates = $date;

						
						    	

						    	echo"<tr>
						    	<td>BILL</td><td>$payable.00</td>
						    	<tr>
						    	<tr>
						    	<td>RECIEVED</td>
						    	<td>$paid.00</td>
						    	</tr>
						    	<tr>
						    	<td>BALANCE</td>
						    	<td>$balance.00</td>
						    	</tr>
						    	
					    </tr>
					    			"

					    			;

					

						
}

					$stmt->close();

  			?>
  		</table>
  		</div>



								</div>
							</div>
						</div> 
					</div>
				</div>
			</div>