<?php
require_once '../config/connect.php';

?>

<div class="form-group custom-input-space has-feedback">
				<div class="page-heading">
					<h3 class="post-title"></h3>
				</div>
				<div class="page-body clearfix">
					<div class="row">
						<div class="col-md-offset-0 col-md-12">
							<div class="panel panel-default">
								<div class="panel-heading " style="background-color: #5cb85c !important" >
										<center>
										<div class="btn btn-primary" style="background-color: #5cb85c !important;border-color: #5cb85c !important">WEEKLY ACCOUNTING REPORT </div>
										</center>

								 	</div>
								
								
								<div class="panel-body">
									<table class="table table-hover table-responsive table-editable" id="dashy">
					    	
					    	<div class="btn-group dropright">
						  <button type="button" class="btn btn-success dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    SELECT MONTH
						  </button>
						  <div class="dropdown-menu ">
						  	<?php
						  	$year = date('Y');
						  	$stmt = $con->prepare("SELECT EXTRACT(MONTH FROM date),MONTHNAME(date) from dental_record group by MONTHNAME(date) desc");
						  	$stmt->execute();
							$stmt->store_result();
							$stmt->bind_result($mont_num,$month_name);
							while($stmt->fetch()) {
								
								
						    				echo'

						    <a class="dropdown-item " href="admindashboard.php?action=weekly&week='.$mont_num.'">'.$month_name	.'</a>';
		
						    				
						  	
						    }
						    $stmt->close();
						    
						    

						    ?>
						</div>


					</div>


					    	<thead>
					    		
					    		<tr>
					    		 <th scope="col">WEEK NO.</th>
					    		 <th scope="col">DAY</th>
					    		 <th scope="col">TOTAL BILL</th>
					    		 <th scope="col">CASH ON HAND</th>
					    		 <th scope="col">ACCOUNTS RECIEVABLE</th>
							      
							      
					    		</tr>

					    	</thead>
					    	<tbody>
					    		
					<?php


					function getStartAndEndDate($week, $year) {
		   				$dto = new DateTime();
		 				$dto->setISODate($year, $week);
		 				$ret['week_start'] = $dto->format('d/m/Y');
		  				$dto->modify('+6 days');
		  				$ret['week_end'] = $dto->format('d/m/Y');
		  				return $ret;
				}

				$month = date('m');
				if(!isset($_GET['week'])){
					$weeky = $month;
				}else{
					$weeky = $_GET['week'];
				}
					
					$total_bill = 0;
					$total_balance = 0;
					$total_paid = 0;
										$stmt = $con->prepare("SELECT WEEK(date),date,MONTHNAME(date),payable,balance,paid from dental_record where EXTRACT(MONTH FROM date) = ?");
					$stmt->bind_param("i", $weeky);
					$stmt->execute();
					$stmt->store_result();
					if($stmt->num_rows === 0) {
						echo "No Data Found";
						
						//header("Location:../user/admindashboard.php");
					}

					$stmt->bind_result(
						$week_no, $date,$month,
						$weekly_bill,
						$weekly_balance,
						$weekly_paid
					
						
						
						
						); 
					
					$i = 0;
					$j = 0;
					$week_nos = 0;
					$day_nos = 0;

function weekOfMonth($date) {
    // estract date parts
    list($y, $m, $d) = explode('-', date('Y-m-d', strtotime($date)));

    // current week, min 1
    $w = 1;

    // for each day since the start of the month
    for ($i = 1; $i <= $d; ++$i) {
        // if that day was a sunday and is not the first day of month
        if ($i > 1 && date('w', strtotime("$y-$m-$i")) == 0) {
            // increment current week
            ++$w;
        }
    }

    // now return
    return $w;
}
		$st = "";			
while($stmt->fetch()) {
	$i++;
	$j++;
	$total_bill = $total_bill + $weekly_bill;
	$total_paid = $total_paid + $weekly_paid;
	$total_balance = $total_balance + $weekly_balance;

	if($week_no>$week_nos){
			$i = 0; 
		}
		$week_nos = $week_no;//

	if($date>$day_nos){
			$j = 0; 
		}
		$day_nos = $date;//	
		$week_array = getStartAndEndDate($week_no,$year);

		$weekMonth = weekOfMonth($date);
		if($weekMonth ===1){
			$st = "st";
		}
		elseif($weekMonth === 2){
			$st = "nd";
		}
		elseif($weekMonth === 3){
			$st = "rd";
		}
		else{
			$st = "th";
		}
					echo"<tr>";

							if($i == 0){
						    	echo"<td><b>".$weekMonth."".$st." week of $month</b><br/></td>";
						    	}
						    else{
						    	echo"<td>-</td>";
						    }	
						    	
						    if($j == 0){
						    	echo"<td>$date";
						    }
						    else{
						    	echo"<td>-</td>";
						    }
						    		

						    			
					echo"		
						    	</td>
						    	<td>$weekly_bill</td>
						    	<td>$weekly_paid</td>
						    	<td>$weekly_balance</td>
					    </tr>
					    			"

					    			;

					}




					$stmt->close();


					
					echo"
								<tr>
					    			<th>Total</th>
					    			<th></th>
					    			<th>".$total_bill."</th>
					    			<th>".$total_paid."</th>
					    			<th>".$total_balance."</th>
					    		</tr>
					    		
					";

//$firstday = date('Y-m-d', strtotime("this week")); 
//$lastday = date('Y-m-d', strtotime("this week +6 day"));  
					?>	

								
					    			

					    	</tbody>
					    
					  </table>



								</div>
							</div>
						</div> 
					</div>
				</div>
			</div>
