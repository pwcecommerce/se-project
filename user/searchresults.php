<?php
require_once '../config/connect.php';
if (isset(($_SESSION['q']))) {
	
$q = $_SESSION['q'];//Query
}
?>

<div class="form-group custom-input-space has-feedback">
				<div class="page-heading">
					<h3 class="post-title"></h3>
				</div>
				<div class="page-body clearfix">
					<div class="row">
						<div class="col-md-offset-0 col-md-12">
							<div class="panel panel-default">
								<div class="panel-heading " style="background-color: #5cb85c !important" >
										<center>
										<div class="btn btn-primary" style="background-color: #5cb85c !important;border-color: #5cb85c !important">Patients Record </div>
										</center>

								 	</div>
								
								
								<div class="panel-body">
									<table class="table table-hover table-responsive table-editable" id="dashy">
					    	
					    	<thead>
					    		<tr>
					    		 <th scope="col">NAME</th>
					    		 <th scope="col">AGE</th>
					    		 <th scope="col">CONTACT</th>
					    		 <th scope="col">GENDER</th>
							      <th scope="col">ADDRESS</th>
							      <th scope="col">OCCUPATION</th>
							      <th scope="col">STATUS</th>
							      
					    		</tr>

					    	</thead>
					    	<tbody>
					    		
									<?php
										$stmt = $con->prepare("SELECT 
						patient_id,patient_name,
						patient_age,patient_contact,
						patient_gender,patient_address,
						patient_occupation,status,date,time

					 FROM patient_profile WHERE patient_name LIKE CONCAT('%',?, '%')");
					$stmt->bind_param("s", $q);
					$stmt->execute();
					$stmt->store_result();
					if($stmt->num_rows === 0) {
						echo "No Data Found";
						
						//header("Location:../user/admindashboard.php");
					}

					$stmt->bind_result(
						$patient_id,
						$patient_name,
						$patient_age,
						$patient_contact,
						$patient_gender,
						$patient_address,
						$patient_occupation,
						$patient_status,
						$date,
						$time); 
					
while($stmt->fetch()) {
					echo"			<tr>
						    			<td><a href='clientdashboard.php?id=".$patient_id."'>$patient_name</a></td>
						    			<td>$patient_age</td>
						    			<td>$patient_contact</td>
						    			<td>$patient_gender</td>
						    			<td>$patient_address</td>
						    			<td>$patient_occupation</td>
						    			<td>$patient_status</td>
					    			</tr>
					    			"

					    			;

					}




					$stmt->close();


					



					?>	

								
					    			
					    		
					    	</tbody>
					    
					  </table>



								</div>
							</div>
						</div> 
					</div>
				</div>
			</div>
