<?php
require_once '../config/connect.php';

?>

<div class="form-group custom-input-space has-feedback">
				<div class="page-heading">
					<h3 class="post-title"></h3>
				</div>
				<div class="page-body clearfix">
					<div class="row">
						<div class="col-md-offset-0 col-md-12">
							<div class="panel panel-default">
								<div class="panel-heading " style="background-color: #5cb85c !important" >
										<center>
										<div class="btn btn-primary" style="background-color: #5cb85c !important;border-color: #5cb85c !important">WEEKLY PROCEDURE REPORT </div>
										</center>

								 	</div>
								
								
								<div class="panel-body">
									<table class="table table-hover table-responsive table-editable" id="dashy">
					    		<div class="btn-group dropright">
						  <button type="button" class="btn btn-success dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    SELECT MONTH
						  </button>
						  <div class="dropdown-menu ">
						  	<?php
						  	$month = date('m');
						  	$year = date('Y');
						  	$stmt = $con->prepare("SELECT MONTHNAME(date),EXTRACT(MONTH FROM date) from dental_record group by MONTHNAME(date) desc");
						  	$stmt->execute();
							$stmt->store_result();
							$stmt->bind_result($wek,$week);
							while($stmt->fetch()) {
								
								
						    				echo'

						    <a class="dropdown-item " href="admindashboard.php?action=weeklyprocedure&week='.$week.'">'.$wek.'</a>';
		
						    				
						  	
						    }
						    $stmt->close();
						    
						    

						    ?>
						</div>
					</div>

					    	<thead>
					    		<tr>
					    		 <th scope="col">WEEK NO.</th>
					    		 <th scope="col">DATE</th>
					    		 <th scope="col">NO. OF PATIENTS</th>
					    		 <th scope="col">PERCENTAGE</th>
					    		 <th scope="col">PROCEDURE NAME</th>
							      
							      
					    		</tr>

					    	</thead>
					    	<tbody>
					    		
					<?php


					function getStartAndEndDate($week, $year) {
		   				$dto = new DateTime();
		 				$dto->setISODate($year, $week);
		 				$ret['week_start'] = $dto->format('d/m/Y');
		  				$dto->modify('+6 days');
		  				$ret['week_end'] = $dto->format('d/m/Y');
		  				return $ret;
				}


					$total_bill = 0;
					$total_balance = 0;
					$total_paid = 0;

$currentWeekNumber = date('W');
$month = date('m');
				if(!isset($_GET['week'])){
					$weeky = $month;
				}else{
					$weeky = $_GET['week'];
				}
					$stmt1 = $con->prepare("SELECT count(patient_id) from dental_record where EXTRACT(MONTH FROM date) = ? ");
					$stmt1->bind_param("i", $weeky);
						$stmt1->execute();
					$stmt1->store_result();
					if($stmt1->num_rows === 0) {
						echo "No Data Found";	
					}
					$stmt1->bind_result($total);
					$stmt1->fetch();
					
					$stmt1->close();

					

					
					$rate = 0;
		$stmt = $con->prepare("SELECT dental_record.date,MONTHNAME(dental_record.date),WEEK(dental_record.date),COUNT(dental_record.patient_id), dental_procedure.procedure_name from dental_record,dental_procedure where dental_record.procedure_id = dental_procedure.procedure_id and EXTRACT(MONTH FROM dental_record.date) = ? group by EXTRACT(MONTH FROM dental_record.date),dental_record.procedure_id");
					$stmt->bind_param("i", $weeky);
					$stmt->execute();
					$stmt->store_result();
					if($stmt->num_rows === 0) {
						echo "No Data Found for This Week";
						
						//header("Location:../user/admindashboard.php");
					}

					$stmt->bind_result(
						$date,$month,$week_no,
						$weekly_no_patients,
						$weekly_procedure_name		
						
						); 
					
					
				$total_count = 0;
				$i = 0;	
				$j = 0;
				$week_nos = 0;
				$day_nos = 0;
				function weekOfMonth($date) {
    // estract date parts
    list($y, $m, $d) = explode('-', date('Y-m-d', strtotime($date)));

    // current week, min 1
    $w = 1;

    // for each day since the start of the month
    for ($i = 1; $i <= $d; ++$i) {
        // if that day was a sunday and is not the first day of month
        if ($i > 1 && date('w', strtotime("$y-$m-$i")) == 0) {
            // increment current week
            ++$w;
        }
    }

    // now return
    return $w;
}
		$st = "";	
while($stmt->fetch()) {
	$total_count += $weekly_no_patients;
	$year = date('Y');
	$rate = ($weekly_no_patients/$total)*100;
	$i++;
	$j++;
	// $total_bill = $total_bill + $weekly_bill;
	// $total_paid = $total_paid + $weekly_paid;
	// $total_balance = $total_balance + $weekly_balance;
	

		if($week_no>$week_nos){
			$i = 0; 
		}
		$week_nos = $week_no;//
		if($date>$day_nos){
			$j = 0; 
		}
		$day_nos = $date;//	
		if($week_no>$week_nos){
			$i = 0; 
		}
		$week_nos = $week_no;//

		if($date>$day_nos){
			$j = 0; 
		}
		$day_nos = $date;//	
		$weekMonth = weekOfMonth($date);
		if($weekMonth ===1){
			$st = "st";
		}
		elseif($weekMonth === 2){
			$st = "nd";
		}
		elseif($weekMonth === 3){
			$st = "rd";
		}
		else{
			$st = "th";
		}

		$week_array = getStartAndEndDate($week_no,$year);

					echo"<tr>";
					if($i==0){
						    	echo"<td><b>".$weekMonth."".$st." week of $month</b></td>";
						    }
					else{
								echo"<td>-</td>";
					}						    	
						    	if($j == 0){
						    	echo"<td>$date";
						    }
						    else{
						    	echo"<td>-</td>";
						    }

						    		

						    			
					echo"		
						    	</td>
						    	<td>$weekly_no_patients</td>
						    	<td>".number_format($rate,2,'.','')."%</td>
						    	<td>$weekly_procedure_name	</td>
					    </tr>
					    			"

					    			;

					}




					$stmt->close();


					
					echo"
								<tr>
					    			<th>Total</th>
					    			<th></th>
					    			<th>".$total_count."</th>
					    			<th>100</th>
					    		</tr>
					";

//$firstday = date('Y-m-d', strtotime("this week")); 
//$lastday = date('Y-m-d', strtotime("this week +6 day"));  
					?>	

								
					    			

					    	</tbody>
					    
					  </table>



								</div>
							</div>
						</div> 
					</div>
				</div>
			</div>
