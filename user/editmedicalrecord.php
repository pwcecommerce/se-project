<?php
require_once '../config/connect.php';
$recordid = $_GET['recordid'];

?>
<div class="form-group custom-input-space has-feedback">
				<div class="page-heading">
					<h3 class="post-title"></h3>
				</div>
				<div class="page-body clearfix">
					<div class="row">
						<div class="col-md-offset-0 col-md-12">
							<div class="panel panel-default">
								<div class="panel-heading " style="background-color: #5cb85c !important" >
										<center>
										<div class="btn btn-primary" style="background-color: #5cb85c !important;border-color: #5cb85c !important">Edit Medical Record </div>
										</center>

								 	</div>
								
								
<div class="panel-body">
	<form class="form-horizontal pb-2" method="post" action="../model/editmedicalrecord.php">
  		<div class="form-group">
    <?php


    $stmt = $con->prepare("SELECT complain,payable,paid,procedure_id,receipt_no FROM dental_record WHERE record_id = ?");
	$stmt->bind_param("i", $recordid);
	$stmt->execute();
	$stmt->store_result();
	if($stmt->num_rows === 0) exit('No rows');
	$stmt->bind_result($complains,$payables,$paids,$procedure_ids,$receipt_no);
	$stmt->fetch();
	
	$stmt->close();

    					
    echo'
    <div class="col-sm-12 ">
      <input type="hidden" class="form-control" id="patient_id" name="patient_id" placeholder="Patient ID" value="'.$patient_id.'">
    </div>
    <div class="col-sm-12 ">
    <label>Record ID: '.$recordid .' </label>	
    </div>
    <div class="col-sm-12 ">
      <input type="hidden"  class="form-control" id="record_id" name="record_id" placeholder="Enter Record ID" value="'.$recordid.'" >
    </div>
    <div class="col-sm-12 ">
    <label>Select Procedure</label>	
    </div>
    <div class="col-sm-12 ">
      
      <select class="form-control" id="procedure_id" name="procedure_id">';
      
//Query Procedure here
      			//default procedure
					$stmts = $con->prepare("SELECT * FROM dental_procedure where procedure_id = ?");
					$stmts->bind_param("i", $procedure_ids);
					$stmts->execute();
					$stmts->store_result();
					if($stmts->num_rows === 0) {
							echo "No Data";
							//header("Location:../user/admindashboard.php");
						}
						$stmts->bind_result($procedure_id3,$procedure_name3);
								

						$stmts->fetch();
									  		
							echo"<option value='".$procedure_id3."'>".$procedure_name3."</option>
        						";	


											
					$stmts->close();	

								//end



					$stmt = $con->prepare("SELECT * FROM dental_procedure ");
					//$stmt->bind_param("i", $procedure_ids);
					$stmt->execute();
					$stmt->store_result();
					if($stmt->num_rows === 0) {
							echo "No Data";
							//header("Location:../user/admindashboard.php");
						}
						$stmt->bind_result($procedure_id2,$procedure_name2);
								

						while($stmt->fetch()) {
									  		
							echo"<option value='".$procedure_id2."'>".$procedure_name2."</option>
        						";	


											}
					$stmt->close();

        

//End here        
      
echo'
      </select>
    </div>
	<div class="col-sm-12 ">
    <label>Select Dentist</label>	
    </div>
    <div class="col-sm-12 ">
      
      <select class="form-control" id="dentist_id" name="dentist_id">';

      		//Default Doctor
      			$role = 1;
      		$stmt = $con->prepare("SELECT id,name FROM user WHERE role = ? and id = ?");		
			$stmt->bind_param("ii", $role,$dentist_id);		
			$stmt->execute();
					$stmt->store_result();
					if($stmt->num_rows === 0) {
							echo "No Data";
							//header("Location:../user/admindashboard.php");
						}
						$stmt->bind_result($dentist_id,$dentist_name);
						$stmt->fetch();
									  		
							echo"<option value='".$dentist_id."'  name=''>Dr. ".$dentist_name."</option>
        						";	

        						
											
					$stmt->close();
      		//End Doctor

      		
      		$stmt = $con->prepare("SELECT id,name FROM user WHERE role = ?");		
			$stmt->bind_param("i", $role);		
			$stmt->execute();
					$stmt->store_result();
					if($stmt->num_rows === 0) {
							echo "No Data";
							//header("Location:../user/admindashboard.php");
						}
						$stmt->bind_result($dentist_id,$dentist_name);
						while($stmt->fetch()) {
									  		
							echo"<option value='".$dentist_id."'  name=''>Dr. ".$dentist_name."</option>
        						";	

        						
											}
					$stmt->close();



    echo' 
    </select>
    </div>
    <div class="col-sm-12 ">
    <label>Enter Complain</label>	
    </div>
	

    <div class="col-sm-12 ">
      <input type="text" class="form-control" id="complain" name="complain" placeholder="Enter Complain" value="'.$complains.'">
    </div>
    <div class="col-sm-12 ">
    <label>Enter Bill</label>	
    </div>
    <div class="col-sm-12 ">
      <input type="text" class="form-control" id="bill"  name="bill" onkeyup="validateNum(this.value)" placeholder="Enter Bill"value="'.$payables.'">
    </div>
    
    <div class="col-sm-12 ">
    <label>Enter Payment</label>	
    </div>
    <div class="col-sm-12 ">
      <input type="text" class="form-control" id="payment" name="payment" onkeyup="validateNum(this.value)" placeholder="Enter Payment" value="'.$paids.'">
    </div>
    <div class="col-sm-12 ">
    <label>Enter Receipt</label>	
    </div>
    	<div class="col-sm-12 ">
      <input type="text" class="form-control" id="receipt" name="receipt" placeholder="Enter Receipt" value="'.$receipt_no.'">
    </div>

    <div class="col-sm-12 ">
      <input type="hidden" class="form-control" id="encoder_id" name="encoder_id" placeholder="Occupation"
      value="'.$_SESSION['id'].'">
    </div>'
    ;
    ?>
  </div>
  
  
  <div class="form-group">
    <div class="col-sm-offset-0 col-md-12">
      <button type="submit" class="btn btn-success col-md-12 " id="submit" name="submit">Submit</button>
    </div>
  </div>
</form>
								</div>
							</div>
						</div> 
					</div>
				</div>
			</div>

			<script type="text/javascript">
					function validateNum(num){
						submit = document.getElementById("submit");
						if(isNaN(num)){
							document.getElementById("submit").disabled = true;
							document.getElementById("submit").innerText= "Invalid Input";


						}
						else{document.getElementById("submit").disabled = false;
							document.getElementById("submit").innerText= "Add Transaction";
					}
					}

			</script>