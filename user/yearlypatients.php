<?php
require_once '../config/connect.php';

?>

<div class="form-group custom-input-space has-feedback">
				<div class="page-heading">
					<h3 class="post-title"></h3>
				</div>
				<div class="page-body clearfix">
					<div class="row">
						<div class="col-md-offset-0 col-md-12">
							<div class="panel panel-default">
								<div class="panel-heading " style="background-color: #5cb85c !important" >
										<center>
										<div class="btn btn-primary" style="background-color: #5cb85c !important;border-color: #5cb85c !important">YEARLY PATIENT ENTRY </div>
										</center>

								 	</div>
								
								
								<div class="panel-body">
									<table class="table table-hover table-responsive table-editable" id="dashy">
					    		<button type="button" class="btn btn-success dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    SELECT YEAR
						  </button>
						  <div class="dropdown-menu ">
						  	<?php
						  	$stmt = $con->prepare("SELECT extract(year FROM date) from patient_profile group by extract(year FROM date)");
						  	$stmt->execute();
							$stmt->store_result();
							$stmt->bind_result($dat);
							while($stmt->fetch()) {
						  	echo'

						    <a class="dropdown-item" href="admindashboard.php?action=yearlypatients&year='.$dat.'">'.$dat.'</a>';

						    }
						    $stmt->close();
						    ?>
						   </div>
						   </div> 
					    	<thead>
					    		<tr>
					    		<th scope="col">YEAR</th>
					    		<th scope="col">MONTH</th>
					    		 <th scope="col">NO. OF PATIENTS</th>
					    		 <th scope="col">PERCENTAGE</th>
					    		 
							      
							      
					    		</tr>

					    	</thead>
					    	<tbody>
					    		
					<?php
					if(isset($_GET['year'])){
						$year = $_GET['year'];
					}
					else{
						$year = date('Y');
					}
					$stmt1 = $con->prepare("SELECT count(patient_id) from patient_profile where extract(year FROM date) = ?");
					$stmt1->bind_param("i", $year);
					$stmt1->execute();
					$stmt1->store_result();
					if($stmt1->num_rows === 0) {
						echo "No Data Found";	
					}
					$stmt1->bind_result($total);
					$stmt1->fetch();
					
					$stmt1->close();

					
					$rate = 0;

							
					
$stmt = $con->prepare("SELECT MONTHNAME(date),extract(year from date),COUNT(patient_id)from patient_profile where extract(year FROM date) = ? group by MONTHNAME(date)");



					$stmt->bind_param("i", $year);
					$stmt->execute();
					$stmt->store_result();
					if($stmt->num_rows === 0) {
						echo "No Data Found";
						
						//header("Location:../user/admindashboard.php");
					}

					$stmt->bind_result(
						$month,$year,
						$yearly_total_patient
						
						
						
						
						
						); 
				$i = 0;	
				$years = 0;
				
while($stmt->fetch()) {
	$rate = ($yearly_total_patient/$total)*100;
	
	$i++;
	
	if($year>$years){
			 $i = 0;
		}
		$years = $year;//
					echo"<tr>";
						    	

								if($i == 0){
						    	echo"<td>$year</td>";
						    	}
						    	else{
						    		echo"<td>-</td>";
						    		
						    	}
						    	echo"<td>$month</td>";
						    	echo"
						    	<td>$yearly_total_patient</td>
						    	<td>".number_format($rate,2,'.','')."%</td>
						    	
						    	
					    </tr>
					    			"

					    			;

					}




					$stmt->close();


					
					echo"
								<tr>
					    			<th>Total</th>
					    			<th></th>
					    			<th>$total</th>
						    	<th>100%</th>
						    	<th></th>
					    		</tr>
					";


					?>	

								
					    			

					    	</tbody>
					    
					  </table>



								</div>
							</div>
						</div> 
					</div>
				</div>
			</div>
