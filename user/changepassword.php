<?php
require_once '../config/connect.php';
$id = $_GET['id'];

?>
<div class="form-group custom-input-space has-feedback">
				<div class="page-heading">
					<h3 class="post-title"></h3>
				</div>
				<div class="page-body clearfix">
					<div class="row">
						<div class="col-md-offset-0 col-md-12">
							<div class="panel panel-default">
								<div class="panel-heading " style="background-color: #5cb85c !important" >
										<center>
										<div class="btn btn-primary" style="background-color: #5cb85c !important;border-color: #5cb85c !important">


										Change Password </div>
										</center>

								 	</div>
								
								
<div class="panel-body">
	<form class="form-horizontal pb-2" method="post" action="../model/changepassword.php">
  		<div class="form-group">
  

    					
   
    
    <div class="col-sm-12 pb-2">
      
  

      
    </div>

    <div class="col-sm-12 pb-2">
      
     



    <div class="col-sm-12 pb-2">
      <?php echo'<input type="hidden" class="form-control" id="id" name="id" placeholder="id" value="'.$id.'">';
      ?>
    </div>
    <div class="col-sm-12 pb-2">
      <input type="password" class="form-control" id="password" name="password" placeholder="New Password" value="">
    </div>
    <div class="col-sm-12 pb-2">
      <input type="password" class="form-control" id="confirm_password" name="confirm_password" placeholder="Confirm Password" value="">
    </div>
    
   
  </div>
  
  
  <div class="form-group">
    <div class="col-sm-offset-3 col-sm-12">
      <button type="submit" class="btn btn-success col-sm-6">SUBMIT</button>
    </div>
  </div>
</form>
								</div>
							</div>
						</div> 
					</div>
				</div>
			</div>

<script type="text/javascript">
	
	var password = document.getElementById("password") 
  , confirm_password = document.getElementById("confirm_password"); // get password value

function validatePassword(){
  if(password.value != confirm_password.value) {
    confirm_password.setCustomValidity("Passwords Don't Match"); //if password does not match
  } else {
    confirm_password.setCustomValidity('');
  }
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;
</script>