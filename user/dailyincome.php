<?php
require_once '../config/connect.php';

?>

<div class="form-group custom-input-space has-feedback">
				<div class="page-heading">
					<h3 class="post-title"></h3>
				</div>
				<div class="page-body clearfix">
					<div class="row">
						<div class="col-md-offset-0 col-md-12">
							<div class="panel panel-default">
								<div class="panel-heading " style="background-color: #5cb85c !important" >
										<center>
										<div class="btn btn-primary" style="background-color: #5cb85c !important;border-color: #5cb85c !important">
											DAILY ACCOUNTING REPORT
											

										</div>
										</center>

								 	</div>
								
								
								<div class="panel-body">
									<table class="table table-hover table-responsive table-editable" id="dashy">
					    	
						<div class="btn-group dropright">
						  <button type="button" class="btn btn-success dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    SELECT DATE
						  </button>
						  <div class="dropdown-menu ">
						  	<?php
						  	$todas = date("Y-m-d");
						  	$stmt = $con->prepare("SELECT date from dental_record group by date");
						  	$stmt->execute();
							$stmt->store_result();
							$stmt->bind_result($dat);
							echo'

						    <a class="dropdown-item" href="admindashboard.php?action=daily&date='.$todas.'">Today</a>';
							while($stmt->fetch()) {
						  	echo'

						    <a class="dropdown-item" href="admindashboard.php?action=daily&date='.$dat.'">'.$dat.'</a>';

						    }
						    $stmt->close();
						    
						    

						    ?>
						  </div>
						 

						 
						</div>


					    	<thead>
					    		<tr>
					    		 <th scope="col">DATE</th>
					    		 <th scope="col">TIME</th>
					    		 
					    		 <th scope="col">TOTAL BILL</th>
					    		 <th scope="col">PAID</th>
					    		 <th scope="col">BALANCE</th>
					    		 <th scope="col">PATIENT NAME</th>
					    		 
							      
							      
					    		</tr>

					    	</thead>
					    	<tbody>
					    		
					<?php
					if(!isset($_GET['date'])){
					$today = date("Y-m-d");
				}else{
					$today = $_GET['date'];
				}
$stmt = $con->prepare("SELECT dental_record.patient_id,dental_record.date,
							  dental_record.time,
			                  dental_record.receipt_no,
							  SUM(dental_record.payable),
							  SUM(dental_record.paid),
							  SUM(dental_record.balance),
						  	  patient_profile.patient_name 
						 from patient_profile,dental_record where patient_profile.patient_id = dental_record.patient_id and dental_record.date = ? group by patient_profile.patient_name");
					$stmt->bind_param("s", $today);
					$stmt->execute();
					$stmt->store_result();
					if($stmt->num_rows === 0) {
						echo "No Data Found For Current Date";
						
						//header("Location:../user/admindashboard.php");
					}

					$stmt->bind_result(
						$id,
						$date,$time,$receipt_no,
						$payable,
						$paid,
						$balance,
						$patient_name
						); 
					
					$total_bill = 0;
					$total_balance = 0;
					$total_paid = 0;	

					$i = 0;
					$d = mktime(8, 12, 2014);
					$daily_dates = "2000-1-12";
while($stmt->fetch()) {
	$total_bill = $total_bill + $payable;
	$total_paid = $total_paid + $paid;
	$total_balance = $total_balance + $balance;

	$i++;
if($date > $daily_dates){
	
	$i = 0;
}
$daily_dates = $date;

						if($i==0){
						    	echo"

						    	<td>$date</td>";
						    	}
						    	else{
						    		echo"

						    	<td>-</td>";
						    	}

						    	echo"<td>$time</td>
						    	<td>$payable</td>
						    	<td>$paid</td>
						    	<td>$balance</td>
						    	<td><a href='clientdashboard.php?id=$id'>$patient_name</a></td>
						    	
					    </tr>
					    			"

					    			;

					}

						echo"<tr>
						    	<th>TOTAL</th>
						    	<th></th>
						    	<th>$total_bill</th>
						    	
						    	<td>$total_paid</td>
						    	<td>$total_balance</td>
					    </tr>

						";


					$stmt->close();


					



					?>	

								
					    			
					    		
					    	</tbody>
					    
					  </table>



								</div>
							</div>
						</div> 
					</div>
				</div>
			</div>
