<?php
ob_start();

	      require_once("../layouts/header.php");
require_once '../config/connect.php';

if(isset($_SESSION['is_admin'])){// check if is_admin variable has value
		$is_admin = $_SESSION['is_admin'];// if true, assign session value to $is_admin variable
	}
	else{
		header("Location:../index.php");// if false redirect to login page or index
	}

$_SESSION['home_link'] = 'admindashboard.php';



?>


	<?php require_once("../layouts/navbar.php");  ?>


	<section class="">
			<div class="form-group custom-input-space has-feedback">
				<div class="page-heading">
					<h3 class="post-title"></h3>
				</div>
				<div class="page-body clearfix">
					<div class="row">
						<div class="col-md-offset-0 col-md-12">
							<div class="panel panel-default">
								<div class="panel-heading " style="background-color: #5cb85c !important" >
										<center>
										<div class="btn btn-primary" style="background-color: #5cb85c !important;border-color: #5cb85c !important">Patient Dashboard </div>
										</center>

								 	</div>
								
								
								<div class="panel-body">

									<div class="col-sm-9">

									


					<?php
					
					if(isset($_GET['id'])){//check if id variable has value
						$id = $_GET['id'];
					}
					else{
						header("Location:admindashboard.php");
					}
					


//Query patient profile
					$stmt = $con->prepare("SELECT 
						patient_id,patient_name,
						patient_age,patient_contact,
						patient_gender,patient_address,
						patient_occupation,status,date,time

					 FROM patient_profile WHERE patient_id = ?");
					$stmt->bind_param("i", $id);
					$stmt->execute();
					$stmt->store_result();
					if($stmt->num_rows === 0) {
						echo "No Data Found";
						
						header("Location:admindashboard.php");
					}

					$stmt->bind_result(
						$patient_id,
						$patient_name,
						$patient_age,
						$patient_contact,
						$patient_gender,
						$patient_address,
						$patient_occupation,
						$patient_status,
						$date,
						$time); 
					$stmt->fetch();
					$stmt->close();


					



					?>				



				
					  <table class="table table-hover table-responsive table-editable" id="dashy">
					    	
					    	<thead>
					    		<tr>
					    		 <th scope="col">NAME</th>
					    		 <th scope="col">AGE</th>
					    		 <th scope="col">CONTACT</th>
					    		 <th scope="col">GENDER</th>
							      <th scope="col">ADDRESS</th>
							      <th scope="col">OCCUPATION</th>
							      <th scope="col">STATUS</th>
							      
					    		</tr>

					    	</thead>
					    	<tbody>
					    		<tr>
					    			<?php
					    			echo"
					    			<td>$patient_name</td>
					    			<td>$patient_age</td>
					    			<td>$patient_contact</td>
					    			<td>$patient_gender</td>
					    			<td>$patient_address</td>
					    			<td>$patient_occupation</td>
					    			<td>$patient_status</td>

					    			"

					    			;
					    			?>
					    		</tr>
					    	</tbody>
					    
					  </table>
					<br/><br/>


<?php


 ?>



				  <table class="table table-hover table-responsive" id="dashy">
					    	
					    	<thead>
					    		<tr>
					    		 <th scope="col">INVOICE</th>
					    		 <th scope="col">RECIEPT#</th>
					    		 <th scope="col">DATE</th>
					    		 <th scope="col">TIME</th>
							      <th scope="col">PROCEDURE</th>
							      <th scope="col">COMPLAIN</th>
							      <th scope="col">PAYABLE</th>
							      <th scope="col">PAID</th>
							      <th scope="col">BALANCE</th>
							      <th scope="col">ENCODED BY</th>
							     

					    		</tr>

					    	</thead>
					    	<tbody>
					    		<?php 
					    		//Query Dental Record Table to Get Necessary IDs;
									$stmt = $con->prepare("SELECT * FROM dental_record WHERE patient_id = ? order by receipt_no");
									$stmt->bind_param("i", $id);
									$stmt->execute();
									$stmt->store_result();
									if($stmt->num_rows === 0) {
										echo "No Data";
										//header("Location:../user/admindashboard.php");
									}

									$stmt->bind_result($record_id,$patient_id,$procedure_id,$dentist_id,$complain,$payable,$date,$time,$encoded_by,$paid,$balance,$receipt_no,$week_no); 

									$total_paid = 0;
									$total_payable = 0;
									$total_balance = 0;

									while($stmt->fetch()) {
									  		
									  		//Please add Paid here and Unpaid Balance here
											$total_payable +=$payable;
											$total_paid +=$paid;
											$total_balance += $balance;
											//Query Encoder's Name
												$enc = $con->prepare("SELECT name FROM user WHERE id = ?");
												$enc->bind_param("i", $encoded_by);
												$enc->execute();
												$enc->store_result();
											if($enc->num_rows > 0) {
												$enc->bind_result($encoder_name);
												$enc->fetch();
												$enc->close();
													//header("Location:../user/admindashboard.php");
											}

											$enc = $con->prepare("SELECT procedure_name FROM dental_procedure WHERE procedure_id = ?");
												$enc->bind_param("i", $procedure_id);
												$enc->execute();
												$enc->store_result();
											if($enc->num_rows > 0) {
												$enc->bind_result($procedure_name);
												$enc->fetch();
												$enc->close();
													//header("Location:../user/admindashboard.php");

											}	



									  echo"

					    				<tr class=''>
					    				<td>"; 
					    				if($payable == 0){

					    				}
					    				else{
					    			echo"	<a href='displayinvoice.php?recordid=".$record_id."' target='_blank'>Print Invoice ";
					    			}
					    				echo"</td>
					    				<td>$receipt_no</td>
					    				<td>$date</td>
					    				<td>$time</td>";

					    				

					    				echo"<td>$procedure_name</td>";
					    				
					    				echo"
					    				<td>$complain</td>";
					    				if($payable == 0){
					    					$paid = "N/A";
					    					$balance = "N/A";
					    					echo"<td>N/A</td>";
					    				}
					    				else{
					    				echo"<td>$payable</td>";
					    				}
					    				echo"<td>$paid</td>		
					    				<td>$balance</td>			    				
					    				<td>$encoder_name &nbsp;";

if($_SESSION['is_admin']===1){
echo"<a href='clientdashboard.php?id=".$id."&action=editrecord&recordid=".$record_id."'>Edit</a>";
}

					    				echo"</td>
					    				

					    			</tr>

					    			

					    			";
									 
									}

									
										echo"
											<tr class='success'>
					    				<td> </td>
					    				<td> </td>
					    				<td> </td>
					    				<td> </td>
					    				<td> </td>
					    				<th> $total_payable</th>
					    				<th> $total_paid</th>
					    				<th> $total_balance</th>
					    				<td> </td>
					    				    				
					    				
					    				

					    			</tr>

										";


									$stmt->close();
									//End of Query
					    			
					    		
					    		?>

					    		
					    	</tbody>

					    	<thead>
					    		<tr>
					    		 <th scope="col">INVOICE</th>
					    		 <th scope="col">DATE</th>
					    		 <th scope="col">TIME</th>
							      <th scope="col">PROCEDURE</th>
							      <th scope="col">COMPLAIN</th>
							      <th scope="col">PAYABLE</th>
							      <th scope="col">PAID</th>
							      <th scope="col">BALANCE</th>
							      <th scope="col">ENCODED BY</th>
							     

					    		</tr>

					    	</thead>

					    
					  </table>



					</div> <!-- First Column-->
					<div class="col-sm-3">
						<div class="btn-group dropright">
						  <button type="button" class="btn btn-success dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    Option
						  </button>
						  <div class="dropdown-menu ">
						  	<?php
						  	echo'
						    <a class="dropdown-item" href="clientdashboard.php?id='.$id.'&action=addtransaction">Add Transaction</a>';

						    if($_SESSION['is_admin']===1){
						    echo'<a class="dropdown-item" href="clientdashboard.php?id='.$id.'&action=editprofile">Edit</a>';
						    
						    echo'<a class="dropdown-item" href="../model/deleteprofile.php?id='.$id.'">Delete</a>';
						    
						    }

						    ?>
						  </div>
						  <?php if(isset($_SESSION['message'])){
						  	echo $_SESSION['message'];
						  }


						  ?>
						</div>


						<?php
							if(isset($_GET['action'])){
								if($_GET['action'] === 'addtransaction'){
									require_once('transactionform.php');
								}
								elseif($_GET['action'] === 'editprofile'){
									if($_SESSION['is_admin']===1){
									require_once('editprofile.php');}
									else{echo"<br/>Sorry Only an Admin can edit any profile";}
								}
								elseif($_GET['action'] === 'editrecord'){
									require_once('editmedicalrecord.php');
								}
								else{
									echo "Please Add Valid Action";
								}

							}
							else{
								echo "<br/><b>You can select actions to here</b>";
							}

						  ?>

													
					</div>
						

					</div> 

					
									
								


						
					</div>


				</div>
				</div>
			</div>
		</section>
</body>