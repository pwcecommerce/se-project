<?php
require_once '../config/connect.php';
?>

<div class="form-group custom-input-space has-feedback">
				<div class="page-heading">
					<h3 class="post-title"></h3>
				</div>
				<div class="page-body clearfix">
					<div class="row">
						<div class="col-md-offset-0 col-md-12">
							<div class="panel panel-default">
								<div class="panel-heading " style="background-color: #5cb85c !important" >
										<center>
										<div class="btn btn-primary" style="background-color: #5cb85c !important;border-color: #5cb85c !important">DAILY PATIENT ENTRY </div>
										</center>

								 	</div>
								
								
								<div class="panel-body">
									<table class=" col-md-12 table table-hover table-responsive table-editable" id="dashy">
					    	
										<div class="btn-group dropright">
						  <button type="button" class="btn btn-success dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    SELECT DATE
						  </button>
						  <div class="dropdown-menu ">
						  	<?php
						  	$todas = date("Y-m-d");
						  	if(!isset($_GET['date'])){
					$today = date("Y-m-d");
				}else{
					$today = $_GET['date'];
				}		
						  	$stmt = $con->prepare("SELECT date from patient_profile group by date");
						  	$stmt->execute();
							$stmt->store_result();
							$stmt->bind_result($dat);
							echo'<a class="dropdown-item" href="admindashboard.php?action=dailypatients&date='.$todas.'">Today</a>';
							while($stmt->fetch()) {
						  	echo'

						    <a class="dropdown-item" href="admindashboard.php?action=dailypatients&date='.$dat.'">'.$dat.'</a>';

						    }
						    $stmt->close();
						    
						    

						    ?>
						  </div>
						 </div>

					    	<thead>
					    		<tr>
					    			<th scope="col">DATE</th>
					    		 <th scope="col">NAME</th>
					    		 
							      <th scope="col"></th>
							      
					    		</tr>

					    	</thead>
					    	<tbody>
					    		
									<?php
							
									$patientProfile = 'patient_profile';
										$stmt = $con->prepare("SELECT 
						patient_id,patient_name,
						patient_age,patient_contact,
						patient_gender,patient_address,
						patient_occupation,status,date,time

					 FROM patient_profile where date = ? ");
					$stmt->bind_param("s", $today);
					$stmt->execute();
					$stmt->store_result();
					if($stmt->num_rows === 0) {
						echo "No New Patients for Today ";
						
						//header("Location:../user/admindashboard.php");
					}

					$stmt->bind_result(
						
						$patient_id,
						$patient_name,
						$patient_age,
						$patient_contact,
						$patient_gender,
						$patient_address,
						$patient_occupation,
						$patient_status,
						$date,
						$time); 
					
$today = date("Y-m-d");
$total = 0;
$new = 0;

					$i = 0;
					$d = mktime(8, 12, 2014);
					$daily_dates = "2000-1-12";
	while($stmt->fetch()) {
		$total++;
		$i++;
	if($date > $daily_dates){
		
		$i = 0;
	}
	$daily_dates = $date;
					echo"			<tr>";
						if($i == 0){
									echo"<td>$date</td>";
									}
									  else{
									  	echo "<td>-</td>";
									  }
									echo"
						    			<td><a href='clientdashboard.php?id=".$patient_id."'>$patient_name</a></td>";
						    			
						    			if($today === $date){
						    				$new++;
						    			echo"<td>New</td>";
						    		}
						    		else{
						    			echo"<td></td>";
						    		}
						    	
						    	echo"		
					    			</tr>
					    			"

					    			;

					}


							echo "<tr>
									<th>Total Number of Patients:</th>
									<td>".$total."</td>
							</tr>
							<tr>
									<th>Total New Patients:</th>
									<td>".$new."</td>
							</tr>
							<tr>";	

							if($total === 0){
								$rate = 0;
							}
							else{
							$rate = ($new/$total)*100;	
						}
								// echo"	<th>New Patient's Percentage:</th>
								// 	<td>".number_format($rate,2,'.','')."%</td>
							echo"</tr>";	

					$stmt->close();


					



					?>	

								
					    		
					    		
					    	</tbody>
					    
					  </table>



								</div>
							</div>
						</div> 
					</div>
				</div>
			</div>
