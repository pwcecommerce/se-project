  <div class="form-group custom-input-space has-feedback">
				<div class="page-heading">
					<h3 class="post-title"></h3>
				</div>
				<div class="page-body clearfix">
					<div class="row">
						<div class="col-md-offset-0 col-md-12">
							<div class="panel panel-default">
								<div class="panel-heading " style="background-color: #5cb85c !important" >
										<center>
										<div class="btn btn-primary" style="background-color: #5cb85c !important;border-color: #5cb85c !important">Add New Record </div>
										</center>

								 	</div>
								
								
								<div class="panel-body">
<form class="form-horizontal pb-2" method="post" action="../model/adduser.php">
  <div class="form-group">
   
    <div class="col-sm-12 pb-2">
      <input type="text" class="form-control" id="name" name="name" placeholder="Full Name">
    </div>
    
    <div class="col-sm-12 pb-2">
      <input type="text" class="form-control" id="contact" name="contact" placeholder="Contact" >
    </div>
    
    <div class="col-sm-12 pb-2">
      <input type="text" class="form-control" id="address" name="address" placeholder="Address">
    </div>
    <div class="col-sm-12 pb-2">
      <input type="text pb-2" class="form-control" id="username" name="username" placeholder="Username">
    </div>
    <div class="col-sm-12 pb-2">
      <input type="password" class="form-control" id="password" name="password" placeholder="Password" >
      
    </div>
    <div class="col-sm-12 pb-2">
      <input type="password" class="form-control" id="confirm_password" name="confirm_password" placeholder="Confirm Password" >
      
    </div>

    <div class="col-sm-12 pb-2">
    <select class="form-control" name="role" id="role">
    
    <option class="form-control" value="1">Dentist</option> 
    <option class="form-control" value="2">Staff</option> 
      </select>
    </div>
    <div class="col-sm-12 pb-2">
    <select class="form-control" name="admin" id="admin">
    
    <option class="form-control" value="1">Admin</option> 
    <option class="form-control" value="0">User</option> 
      </select>
    </div>
   
  </div>
  
  
  <div class="form-group">
    <div class="col-sm-offset-5 col-sm-10">
      <button type="submit" class="btn btn-default">Submit</button>
    </div>
  </div>
</form>
								</div>
							</div>
						</div> 
					</div>
				</div>
			</div>

      <script type="text/javascript">
  
  var password = document.getElementById("password")
  , confirm_password = document.getElementById("confirm_password"); //Get Password Value

function validatePassword(){
  if(password.value != confirm_password.value) { //if password and confirm password does not match
    confirm_password.setCustomValidity("Passwords Don't Match");
  } else {
    confirm_password.setCustomValidity('');
  }
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;
</script>