<?php
require_once '../config/connect.php';

?>

<div class="form-group custom-input-space has-feedback">
				<div class="page-heading">
					<h3 class="post-title"></h3>
				</div>
				<div class="page-body clearfix">
					<div class="row">
						<div class="col-md-offset-0 col-md-12">
							<div class="panel panel-default">
								<div class="panel-heading " style="background-color: #5cb85c !important" >
										<center>
										<div class="btn btn-primary" style="background-color: #5cb85c !important;border-color: #5cb85c !important">DAILY PROCEDURE REPORT </div>
										</center>

								 	</div>
								
								
								<div class="panel-body">
									<table class="table table-hover table-responsive table-editable" id="dashy">

					<div class="btn-group dropright">
						  <button type="button" class="btn btn-success dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    SELECT DATE
						  </button>
						  <div class="dropdown-menu ">
						  	<?php 
						  	$todas = date("Y-m-d");
						  	if(!isset($_GET['date'])){
					$today = date("Y-m-d");
				}else{
					$today = $_GET['date'];
				}
						  	$stmt = $con->prepare("SELECT date from dental_record group by date");
						  	$stmt->execute();
							$stmt->store_result();
							$stmt->bind_result($dat);
							echo'

						    <a class="dropdown-item" href="admindashboard.php?action=dailyprocedure&date='.$todas.'">Today</a>';
							while($stmt->fetch()) {
						  	echo'

						    <a class="dropdown-item" href="admindashboard.php?action=dailyprocedure&date='.$dat.'">'.$dat.'</a>';

						    }
						    $stmt->close();
						    
						    

						    ?>
						  </div>
						 </div>

						 
						</div>
					    	
					    	<thead>
					    		<tr>
					    		 <th scope="col">DATE</th>
					    		 <th scope="col">NO. OF PATIENTS</th>
					    		 <th scope="col">PERCENTAGE</th>
					    		 <th scope="col">PROCEDURE NAME</th>
					    		
					    		 
							      
							      
					    		</tr>

					    	</thead>
					    	<tbody>
					    		
					<?php  

						
						$stmt1 = $con->prepare("SELECT count(patient_id) from dental_record where date = ?");
						$stmt1->bind_param("s", $today);
						$stmt1->execute();
					$stmt1->store_result();
					if($stmt1->num_rows === 0) {
						echo "No Data Found";	
					}
					$stmt1->bind_result($total);
					$stmt1->fetch();
					
					$stmt1->close();

			$stmt = $con->prepare("SELECT dental_record.date,COUNT(dental_record.patient_id),dental_record.procedure_id, dental_procedure.procedure_name from dental_record,dental_procedure where dental_record.procedure_id = dental_procedure.procedure_id and dental_record.date = ? group by dental_record.date,dental_record.procedure_id");
					$stmt->bind_param("s", $today);
					$stmt->execute();
					$stmt->store_result();
					if($stmt->num_rows === 0) {
						echo "No Data Found For Today's Date";
						
						//header("Location:../user/admindashboard.php");
					}

					$stmt->bind_result(
						$daily_date,
						$daily_patient_count,
						$procedure_id,
						$daily_procedure
						); 
					
					$total_count = 0;
					$rate = 0;
					$i = 0;
					$d = mktime(8, 12, 2014);
					$daily_dates = "2000-1-12";
while($stmt->fetch()) {
$i++;
if($daily_date > $daily_dates){
	
	$i = 0;
}

$daily_dates = $daily_date;

	$total_count = $total_count + $daily_patient_count;
	$rate = ($daily_patient_count/$total)*100;
					echo"<tr>";
						    	
						    	if($i==0){
						    	echo"<td>$daily_date </td>";
						    	}
						    	else{
						    		echo"<td>-</td>";
						    	}
						    	echo"<td><a href='admindashboard.php?action=fetch-name&data=$procedure_id'>$daily_patient_count</a></td>

						    	<td>".number_format($rate,2,'.','')."%</td>
						    	<td>$daily_procedure</td>
						    	
						    	
					    </tr>
					    			"

					    			;

					}

						echo"<tr>
						    	<th>TOTAL</th>
						    	<th>$total_count</th>";
						    	if($rate === 0){echo"<th>0%</th>";}
						    		else{echo"<th>100%</th>";}
						    	
						    	
						    	echo"<th></th>
					    </tr>

						";


					$stmt->close();


					



					?>	

								
					    			
					    		
					    	</tbody>
					    
					  </table>



								</div>
							</div>
						</div> 
					</div>
				</div>
			</div>
