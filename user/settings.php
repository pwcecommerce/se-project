<?php
require_once("../layouts/header.php");

if(isset($_SESSION['is_admin'])){
		$is_admin = $_SESSION['is_admin'];
		
				if($is_admin !== 1){
					header("Location:admindashboard.php");
				}
		
	}
	else{
		header("Location:../index.php");
	}


//echo 	date("Y-m-d");

?>


	<?php require_once("../layouts/navbar.php"); ?>


	<section class="container">
			<div class="form-group custom-input-space has-feedback">
				<div class="page-heading">
					<h3 class="post-title"></h3>
				</div>
				<div class="page-body clearfix">
					<div class="row">
						<div class="col-md-offset-0 col-md-12">
							<div class="panel panel-default">
								<div class="panel-heading " style="background-color: #5cb85c !important" >
										<center>
										<div class="btn btn-success" style="background-color: #5cb85c !important;">Settings </div>
										</center>

								 	</div>
								
								
								<div class="panel-body">
									<div class="col-sm-3 pt-4">

					 
					    		<div class="col-md-offset-0 col-md-12 pt-3">
					    			
					   <div class="active-cyan-4 mb-3 ">
					    <form class="form-inline" method="post" action="../model/getuser.php">	
 						<input class="form-control col-sm-8 mb-3" type="text" placeholder="Search User"  id="q" name="q" aria-label="Search" >
 						 <button type="submit" class="btn btn-success col-sm-4 col-sm-offset-0 mb-3">Search</button>
 						</form>
 	  <a href="settings.php?action=viewuser" class="btn btn-success col-md-offset-0  col-md-12 mb-3"><label>Display Users</label></a><br/>					
 	  <a href="settings.php?action=adduser" class="btn btn-success col-md-offset-0  col-md-12 mb-3"><label>Add User</label></a><br/>
 	  
      

      <a href="settings.php?action=dentalprocedures" class="btn btn-success col-md-offset-0  col-md-12 mb-3"><label>Add Dental Procedure</label></a><br/>
      	

											

										</div>
					    				 
					    				 
     			
      
      
    
										 
					    		</div>

					    		
					    	
					    	
					    
					</div>

					<div class="col-sm-9">
						<div class="col-md-offset-0 col-md-12" >
					    			

										<?php  //foreach( $codes as $index => $code )
											if(isset($_GET['action'])){
												if($_GET['action'] ==='viewuser')
												{
													require_once('displayuser.php');
												}
												if($_GET['action'] ==='adduser')
												{
													
													require_once('adduser.php');
												}
												if($_GET['action'] ==='edit')
												{
													require_once('edituser.php');
												}
												if($_GET['action'] ==='changepassword')
												{
													require_once('changepassword.php');
												}
												
												
												if($_GET['action'] ==='dentalprocedures')
												{
													require_once('dentalprocedures.php');
												}
												if($_GET['action'] ==='search' && isset($_SESSION['q']))
												{
													require_once('searchresults.php');
												}

											}
											else{
												require_once('displayuser.php');
											}
											
												

										 ?>
					    		</div>
					</div>
										
				</div>			
								


						
					</div>


				</div>
				</div>
			</div>
		</section>
</body>



