<?php
require_once '../config/connect.php';

?>
<div class="form-group custom-input-space has-feedback">
				<div class="page-heading">
					<h3 class="post-title"></h3>
				</div>
				<div class="page-body clearfix">
					<div class="row">
						<div class="col-md-offset-0 col-md-12">
							<div class="panel panel-default">
								<div class="panel-heading " style="background-color: #5cb85c !important" >
										<center>
										<div class="btn btn-primary" style="background-color: #5cb85c !important;border-color: #5cb85c !important">Edit Profile </div>
										</center>

								 	</div>
								
								
								<div class="panel-body">
<form class="form-horizontal pb-2" method="post" action="../model/edituserprofile.php">
  <div class="form-group">
    <?php

          if(isset($_GET['id'])){
            $id = $_GET['id'];
          }

          $userProfile = 'user';
                    $stmt = $con->prepare("SELECT 
            id,name,
            address,contact,
            username,role,is_admin
            

           FROM user WHERE id = ?");
          $stmt->bind_param("i", $id);
          $stmt->execute();
          $stmt->store_result();
          if($stmt->num_rows === 0) {
            echo "No Data Found";
            
            //header("Location:../user/admindashboard.php");
          }

          $stmt->bind_result(
            $user_id,
            $user_name,
            $user_address,
            $user_contact,
            $user_username,   
            $role,$is_admin
            ); 

           // $_SESSION['user_id'] = $user_id;
           // $_SESSION['user_name'] = $user_name;
           // $_SESSION['user_address'] = $user_address;
           // $_SESSION['user_contact'] = $user_contact;
           // $_SESSION['user_username'] = $user_username;
           // $_SESSION['role'] = $role;
          
          
          

$stmt->fetch();
$stmt->close();

if($role === 1){
            $roles = "Dentist";

          }
          else{$roles = "Staff";}
if($is_admin === 1){
            $admins = "Admin";

          }
          else{$admins = "User";}
    			 
    echo'
    <div class="col-sm-12 pb-2">
      <input type="hidden" class="form-control" id="id" name="id" placeholder="ID" value="'.$user_id.'">
      <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="'.$user_name.'">
    </div>
    <div class="col-sm-12 pb-2">
      <input type="text" class="form-control" id="address" name="address" placeholder="Address"value="'.$user_address.'">
    </div>
    <div class="col-sm-12 pb-2">
      <input type="text" class="form-control" id="contact" name="contact" placeholder="Contact" value="'.$user_contact.'">
    </div>
    <div class="col-sm-12 pb-2">
      <input type="text" class="form-control" id="gender"  name="username" placeholder="User Name"value="'.$user_username.'">
    </div>
    
    <div class="col-sm-12 pb-2">
    <select class="form-control" name="role" id="role">
    <option class="form-control" value="'.$role.'">'.$roles.'</option> 
    <option class="form-control" value="1">Dentist</option> 
    <option class="form-control" value="2">Staff</option> 
      </select>
    </div>

    <div class="col-sm-12 pb-2">
    <select class="form-control" name="admin" id="admin">
    <option class="form-control" value="'.$is_admin.'">'.$admins.'</option> 
    <option class="form-control" value="1">Admin</option> 
    <option class="form-control" value="2">User</option> 
      </select>
    </div>



    ';
    ?>
  </div>
  
  
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-success col-sm-10" >Submit</button>
    </div>
  </div>
</form>
								</div>
							</div>
						</div> 
					</div>
				</div>
			</div>