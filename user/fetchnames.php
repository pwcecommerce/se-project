<?php
require_once '../config/connect.php';
?>

<div class="form-group custom-input-space has-feedback">
				<div class="page-heading">
					<h3 class="post-title"></h3>
				</div>
				<div class="page-body clearfix">
					<div class="row">
						<div class="col-md-offset-0 col-md-12">
							<div class="panel panel-default">
								<div class="panel-heading " style="background-color: #5cb85c !important" >
										<center>

<?php
$proc_id = $_GET['data'];
$stmt2 = $con->prepare("SELECT procedure_name from dental_procedure where procedure_id = ?");
$stmt2->bind_param("i",$proc_id);
$stmt2->execute();
$stmt2->store_result();
$stmt2->bind_result($proc_name);
$stmt2->fetch();


?>

										<div class="btn btn-primary" style="background-color: #5cb85c !important;border-color: #5cb85c !important"><?php echo $proc_name ?> </div>
										</center>

								 	</div>
								
								
								<div class="panel-body">
									<table class="table table-hover table-responsive table-editable col-md-12" id="dashy">
					    	
					    	<thead>
					    		<tr>
					    		 <th scope="col">NAME </th>
					    		 
							      
					    		</tr>

					    	</thead>
					    	<tbody>
					    		
									<?php
									
									$patientProfile = 'patient_profile';
	$stmt = $con->prepare("SELECT patient_profile.patient_name,patient_profile.patient_id from patient_profile,dental_record where patient_profile.patient_id = dental_record.patient_id and dental_record.procedure_id = ? group by patient_profile.patient_name
					");
					$stmt->bind_param("i", $proc_id);
					$stmt->execute();
					$stmt->store_result();
					if($stmt->num_rows === 0) {
						echo "No Data Found";
						
						//header("Location:../user/admindashboard.php");
					}

					$stmt->bind_result(
						$patient_name,
						$patient_id
						// $patient_name,
						// $patient_age,
						// $patient_contact,
						// $patient_gender,
						// $patient_address,
						// $patient_occupation,
						// $patient_status,
						// $date,
						// $time
					); 
					
while($stmt->fetch()) {
					echo"			<tr>
						    			<td><a href='clientdashboard.php?id=$patient_id'>$patient_name</a></td>";
						    		// 	<td>$patient_age</td>
						    		// 	<td>$patient_contact</td>
						    		// 	<td>$patient_gender</td>
						    		// 	<td>$patient_address</td>
						    		// 	<td>$patient_occupation</td>
						    		// 	<td>$patient_status</td>
					    			// </tr>
					    			//"

					    			//;

					}




					$stmt->close();


					



					?>	

								
					    			
					    		
					    	</tbody>
					    
					  </table>



								</div>
							</div>
						</div> 
					</div>
				</div>
			</div>
