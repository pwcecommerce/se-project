<?php
//Go to line 46 to continue your task
require_once("../layouts/header.php");
require_once '../config/connect.php';

if(!isset($_GET['recordid'])){
	header("Location:".$_SESSION['current_link']);
}
$id = $_GET['recordid'];

if(isset($_SESSION['is_admin'])){
		$is_admin = $_SESSION['is_admin'];
		
		
	}
	else{
		header("Location:../index.php");
	}

//echo 	date("Y-m-d");

?>


	<?php 
	?>


	<section class="container">
			<div class="form-group custom-input-space has-feedback">
				<div class="page-heading">
					<h3 class="post-title"></h3>
				</div>
				<div class="page-body clearfix">
					<div class="row">
						<div class="col-md-offset-3 col-md-5">
							<div class="panel panel-default">
								<div class="panel-heading " style="background-color: #5cb85c !important" >
										<center>
										<div class="btn btn-success" style="background-color: #5cb85c !important;">Ongkingco Dental Clinic Invoice </div>
										</center>

								 	</div>
								
								
			<div class="panel-body">
				<div class="col-sm-12 pt-4">

					<?php
					//Continue here tomorrow
						$stmt = $con->prepare("SELECT * FROM dental_record WHERE record_id = ?");
									$stmt->bind_param("i", $id);
									$stmt->execute();
									$stmt->store_result();
									if($stmt->num_rows === 0) {
										echo "No Data";
										//header("Location:../user/admindashboard.php");
									}

									$stmt->bind_result($record_id,$patient_id,$procedure_id,$dentist_id,$complain,$payable,$date,$time,$encoded_by,$paid,$balance,$receipt_no,$week_no); 

									$total_paid = 0;
									$total_payable = 0;
									$total_balance = 0;

									while($stmt->fetch()) {
									  		
									  		//Please add Paid here and Unpaid Balance here
											$total_payable +=$payable;
											$total_paid +=$paid;
											$total_balance += $balance;
											//Query Encoder's Name
												$enc = $con->prepare("SELECT name FROM user WHERE id = ?");
												$enc->bind_param("i", $encoded_by);
												$enc->execute();
												$enc->store_result();
											if($enc->num_rows > 0) {
												$enc->bind_result($encoder_name);
												$enc->fetch();
												$enc->close();
													//header("Location:../user/admindashboard.php");
											}

											$enc = $con->prepare("SELECT name FROM user WHERE 	id = ?");
												$enc->bind_param("i", $dentist_id);
												$enc->execute();
												$enc->store_result();
											if($enc->num_rows > 0) {
												$enc->bind_result($dentist_name);
												$enc->fetch();
												$enc->close();
													//header("Location:../user/admindashboard.php");
											}
												

											$enc = $con->prepare("SELECT procedure_name FROM dental_procedure WHERE procedure_id = ?");
												$enc->bind_param("i", $procedure_id);
												$enc->execute();
												$enc->store_result();
											if($enc->num_rows > 0) {
												$enc->bind_result($procedure_name);
												$enc->fetch();
												$enc->close();
													//header("Location:../user/admindashboard.php");
												}
											}



										$patientProfile = 'patient_profile';
										$stmt = $con->prepare("SELECT 
						patient_id,patient_name,
						patient_age,patient_contact,
						patient_gender,patient_address,
						patient_occupation,status,date,time

					 FROM patient_profile where patient_id = ?");
					$stmt->bind_param("i", $patient_id);
					$stmt->execute();
					$stmt->store_result();
					if($stmt->num_rows === 0) {
						echo "No Data Found";
						
						//header("Location:../user/admindashboard.php");
					}

					$stmt->bind_result(
						$patient_id,
						$patient_name,
						$patient_age,
						$patient_contact,
						$patient_gender,
						$patient_address,
						$patient_occupation,
						$patient_status,
						$date,
						$time);
						$stmt->fetch();
						$stmt->close();		


					?>






<!-- $record_id,$patient_id,$procedure_id,$dentist_id,$complain,$payable,$date,$time,$encoded_by,$paid,$balance,$week_no,$receipt_no -->

						<!-- $patient_id,
						$patient_name,
						$patient_age,
						$patient_contact,
						$patient_gender,
						$patient_address,
						$patient_occupation,
						$patient_status,
						$date,
						$time -->
					 
					<div class="col-md-offset-0 col-md-12 pt-3">
						    	<table class="table table-hover table-responsive table-editable col-md-18	" id="dashy">
						    		<?php 
						    		$date = date("m-d-Y");
									$time = date("h:i:sa");			
							echo'
						    		<tr>
						    			<th class="col-md-4">Date/Time:</th>
						    			<td class="col-md-8">'.$date.'----'.$time.'</td>
						    		</tr>
						    		<tr>
						    			<th class="col-md-4">Full Name:</th>
						    			<td class="col-md-8">'.$patient_name.'</td>
						    		</tr>
						    		<tr>
						    			<th class="col-md-4">Address:</th>
						    			<td class="col-md-8">'.$patient_address.'</td>
						    		</tr>
						    		

						    		<tr>
						    			<th class="col-md-4">Date of Operation:</th>
						    			<td class="col-md-8">'.$date.'</td>
						    		</tr>
						    		<tr>
						    			<th class="col-md-4">OR#:</th>
						    			<td class="col-md-8">'.$receipt_no.'</td>
						    		</tr>
						    		
						    		
						    		<tr>
						    			<th class="col-md-4">Procedure:</th>
						    			<td class="col-md-8">'.$procedure_name.'</td>
						    		</tr>
						    		<tr>
						    			<th class="col-md-4">Bill:</th>
						    			<td class="col-md-8">'.$payable.'</td>
						    		</tr>
						    		<tr>
						    			<th class="col-md-4">Paid:</th>
						    			<td class="col-md-8">'.$paid.'</td>
						    		</tr>
						    		<tr>
						    			<th class="col-md-4">Balance:</th>
						    			<th class="col-md-8">'.$balance.'</th>
						    		</tr>
						    		<tr>
						    			<th class="col-md-4">Dentist:</th>
						    			<td class="col-md-8">'.$dentist_name.'</td>
						    		</tr>
						    		<tr>
						    			<th class="col-md-4">Encoded By:</th>
						    			<td class="col-md-8">'.$encoder_name.'</td>
						    		</tr>';
						    		?>
						    		
						    	</table>	
						   
					</div>

			  </div>

					
										
			</div>			
								


						
					</div>


				</div>
				</div>
			</div>
		</div>
	</section>
</body>



