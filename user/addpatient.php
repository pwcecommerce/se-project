<div class="form-group custom-input-space has-feedback">
				<div class="page-heading">
					<h3 class="post-title"></h3>
				</div>
				<div class="page-body clearfix">
					<div class="row">
						<div class="col-md-offset-0 col-md-12">
							<div class="panel panel-default">
								<div class="panel-heading " style="background-color: #5cb85c !important" >
										<center>
										<div class="btn btn-primary" style="background-color: #5cb85c !important;border-color: #5cb85c !important">Add New Record </div>
										</center>

								 	</div>
								
								
								<div class="panel-body">
<form class="form-horizontal pb-2" method="post" action="../model/addrecord.php">
  <div class="form-group">
   
    <div class="col-sm-12 pb-2">
      <input type="text" class="form-control" id="name" name="name" placeholder="Full Name">
    </div>
    <div class="col-sm-12 pb-2">
      <input type="text" class="form-control" id="age" name="age" placeholder="Age">
    </div>
    <div class="col-sm-12 pb-2">
      <input type="text" class="form-control" id="contact" name="contact" placeholder="Contact" >
    </div>
    <div class="col-sm-12 pb-2">
      <select class="form-control" id="gender"  name="gender">
      <option value="Male">Male</option>
      <option value="Female">Female</option>
      </select>
    </div>
    <div class="col-sm-12 pb-2">
      <select class="form-control" id="status"  name="status">
      <option value="Single">Single</option>
      <option value="Married">Married</option>
      <option value="Widow">Widow</option>

      </select>
    </div>
    <div class="col-sm-12 pb-2">
      <input type="text" class="form-control" id="address" name="address" placeholder="Address">
    </div>
    <div class="col-sm-12 pb-2">
      <input type="text pb-2" class="form-control" id="occupation" name="occupation" placeholder="Occupation">
    </div>
    
   
  </div>
  
  
  <div class="form-group">
    <div class="col-sm-offset-2 col-md-10">
      <button type="submit" class="btn btn-success col-md-10">Submit</button>
    </div>
  </div>
</form>
								</div>
							</div>
						</div> 
					</div>
				</div>
			</div>