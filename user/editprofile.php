<div class="form-group custom-input-space has-feedback">
				<div class="page-heading">
					<h3 class="post-title"></h3>
				</div>
				<div class="page-body clearfix">
					<div class="row">
						<div class="col-md-offset-0 col-md-12">
							<div class="panel panel-default">
								<div class="panel-heading " style="background-color: #5cb85c !important" >
										<center>
										<div class="btn btn-primary" style="background-color: #5cb85c !important;border-color: #5cb85c !important">Edit Profile </div>
										</center>

								 	</div>
								
								
								<div class="panel-body">
<form class="form-horizontal pb-2" method="post" action="../model/editprofile.php">
  <div class="form-group">
    <?php
    $id = $_GET['id'];
    					
    echo'
    <div class="col-sm-12 pb-2">
      <input type="hidden" class="form-control" id="id" name="id" placeholder="Full Name" value="'.$id.'">
    </div>
    <div class="col-sm-12 pb-2">
      <input type="text" class="form-control" id="name" name="name" placeholder="Full Name" value="'.$patient_name.'">
    </div>
    <div class="col-sm-12 pb-2">
      <input type="text" class="form-control" id="age" name="age" placeholder="Age"value="'.$patient_age.'">
    </div>
    <div class="col-sm-12 pb-2">
      <input type="text" class="form-control" id="contact" name="contact" placeholder="Contact" value="'.$patient_contact.'">
    </div>
    

      <div class="col-sm-12 pb-2">
      <select class="form-control" id="gender"  name="gender">
      <option value="'.$patient_gender.'">'.$patient_gender.'</option>
      <option value="Male">Male</option>
      <option value="Female">Female</option>
      

      </select>
  
    </div>
    <div class="col-sm-12 pb-2">
      <input type="text" class="form-control" id="address" name="address" placeholder="Address"value="'.$patient_address.'">
    </div>
    <div class="col-sm-12 pb-2">
      <input type="text pb-2" class="form-control" id="occupation" name="occupation" placeholder="Occupation"
      value="'.$patient_occupation.'">
    </div>

    <div class="col-sm-12 pb-2">
      <select class="form-control" id="status"  name="status">
      <option value="'.$patient_status.'">'.$patient_status.'</option>
      <option value="Single">Single</option>
      <option value="Married">Married</option>
      <option value="Widow">Widow</option>

      </select>
    </div>

    ';
    ?>
  </div>
  
  
  <div class="form-group">
    <div class="col-sm-offset-3 col-sm-10">
      <button type="submit" class="btn btn-default">Submit</button>
    </div>
  </div>
</form>
								</div>
							</div>
						</div> 
					</div>
				</div>
			</div>